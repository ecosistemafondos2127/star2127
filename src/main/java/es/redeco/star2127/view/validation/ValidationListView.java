package es.redeco.star2127.view.validation;

import es.redeco.star2127.entity.Validation;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "validations", layout = MainView.class)
@ViewController("Validation.list")
@ViewDescriptor("validation-list-view.xml")
@LookupComponent("validationsDataGrid")
@DialogMode(width = "64em")
public class ValidationListView extends StandardListView<Validation> {
}