package es.redeco.star2127.view.validation;

import es.redeco.star2127.entity.Validation;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "validations/:id", layout = MainView.class)
@ViewController("Validation.detail")
@ViewDescriptor("validation-detail-view.xml")
@EditedEntityContainer("validationDc")
public class ValidationDetailView extends StandardDetailView<Validation> {
}