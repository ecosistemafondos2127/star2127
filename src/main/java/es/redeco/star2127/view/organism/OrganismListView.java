package es.redeco.star2127.view.organism;

import es.redeco.star2127.entity.Organism;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "organisms", layout = MainView.class)
@ViewController("Organism.list")
@ViewDescriptor("organism-list-view.xml")
@LookupComponent("organismsDataGrid")
@DialogMode(width = "64em")
public class OrganismListView extends StandardListView<Organism> {
}