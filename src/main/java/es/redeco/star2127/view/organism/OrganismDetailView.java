package es.redeco.star2127.view.organism;

import es.redeco.star2127.entity.Organism;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "organisms/:id", layout = MainView.class)
@ViewController("Organism.detail")
@ViewDescriptor("organism-detail-view.xml")
@EditedEntityContainer("organismDc")
public class OrganismDetailView extends StandardDetailView<Organism> {
}