package es.redeco.star2127.view.economicactivity;

import es.redeco.star2127.entity.EconomicActivity;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "economicActivities/:id", layout = MainView.class)
@ViewController("EconomicActivity.detail")
@ViewDescriptor("economic-activity-detail-view.xml")
@EditedEntityContainer("economicActivityDc")
public class EconomicActivityDetailView extends StandardDetailView<EconomicActivity> {
}