package es.redeco.star2127.view.economicactivity;

import es.redeco.star2127.entity.EconomicActivity;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "economicActivities", layout = MainView.class)
@ViewController("EconomicActivity.list")
@ViewDescriptor("economic-activity-list-view.xml")
@LookupComponent("economicActivitiesDataGrid")
@DialogMode(width = "64em")
public class EconomicActivityListView extends StandardListView<EconomicActivity> {
}