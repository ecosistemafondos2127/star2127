package es.redeco.star2127.view.nuts;

import es.redeco.star2127.entity.Nuts;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "nutses", layout = MainView.class)
@ViewController("Nuts.list")
@ViewDescriptor("nuts-list-view.xml")
@LookupComponent("nutsesDataGrid")
@DialogMode(width = "64em")
public class NutsListView extends StandardListView<Nuts> {
}