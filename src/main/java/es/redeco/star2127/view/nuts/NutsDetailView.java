package es.redeco.star2127.view.nuts;

import es.redeco.star2127.entity.Nuts;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "nutses/:id", layout = MainView.class)
@ViewController("Nuts.detail")
@ViewDescriptor("nuts-detail-view.xml")
@EditedEntityContainer("nutsDc")
public class NutsDetailView extends StandardDetailView<Nuts> {
}