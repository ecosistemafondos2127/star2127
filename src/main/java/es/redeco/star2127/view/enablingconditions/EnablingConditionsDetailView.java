package es.redeco.star2127.view.enablingconditions;

import es.redeco.star2127.entity.EnablingConditions;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "enablingConditionses/:id", layout = MainView.class)
@ViewController("EnablingConditions.detail")
@ViewDescriptor("enabling-conditions-detail-view.xml")
@EditedEntityContainer("enablingConditionsDc")
public class EnablingConditionsDetailView extends StandardDetailView<EnablingConditions> {
}