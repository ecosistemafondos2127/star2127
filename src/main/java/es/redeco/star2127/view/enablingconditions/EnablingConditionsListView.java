package es.redeco.star2127.view.enablingconditions;

import es.redeco.star2127.entity.EnablingConditions;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "enablingConditionses", layout = MainView.class)
@ViewController("EnablingConditions.list")
@ViewDescriptor("enabling-conditions-list-view.xml")
@LookupComponent("enablingConditionsesDataGrid")
@DialogMode(width = "64em")
public class EnablingConditionsListView extends StandardListView<EnablingConditions> {
}