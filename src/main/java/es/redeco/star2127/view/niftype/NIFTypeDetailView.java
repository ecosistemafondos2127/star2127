package es.redeco.star2127.view.niftype;

import es.redeco.star2127.entity.NIFType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "nIFTypes/:id", layout = MainView.class)
@ViewController("NIFType.detail")
@ViewDescriptor("nif-type-detail-view.xml")
@EditedEntityContainer("nIFTypeDc")
public class NIFTypeDetailView extends StandardDetailView<NIFType> {
}