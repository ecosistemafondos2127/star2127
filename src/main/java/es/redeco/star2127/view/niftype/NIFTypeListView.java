package es.redeco.star2127.view.niftype;

import es.redeco.star2127.entity.NIFType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "nIFTypes", layout = MainView.class)
@ViewController("NIFType.list")
@ViewDescriptor("nif-type-list-view.xml")
@LookupComponent("nIFTypesDataGrid")
@DialogMode(width = "64em")
public class NIFTypeListView extends StandardListView<NIFType> {
}