package es.redeco.star2127.view.fundgroup;

import es.redeco.star2127.entity.FundGroup;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "fundGroups", layout = MainView.class)
@ViewController("FundGroup.list")
@ViewDescriptor("fund-group-list-view.xml")
@LookupComponent("fundGroupsDataGrid")
@DialogMode(width = "64em")
public class FundGroupListView extends StandardListView<FundGroup> {
}