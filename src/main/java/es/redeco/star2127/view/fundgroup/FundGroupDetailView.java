package es.redeco.star2127.view.fundgroup;

import es.redeco.star2127.entity.FundGroup;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "fundGroups/:id", layout = MainView.class)
@ViewController("FundGroup.detail")
@ViewDescriptor("fund-group-detail-view.xml")
@EditedEntityContainer("fundGroupDc")
public class FundGroupDetailView extends StandardDetailView<FundGroup> {
}