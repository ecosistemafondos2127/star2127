package es.redeco.star2127.view.specificobjective;

import es.redeco.star2127.entity.SpecificObjective;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "specificObjectives/:id", layout = MainView.class)
@ViewController("SpecificObjective.detail")
@ViewDescriptor("specific-objective-detail-view.xml")
@EditedEntityContainer("specificObjectiveDc")
public class SpecificObjectiveDetailView extends StandardDetailView<SpecificObjective> {
}