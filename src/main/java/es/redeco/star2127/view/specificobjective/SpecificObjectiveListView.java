package es.redeco.star2127.view.specificobjective;

import es.redeco.star2127.entity.SpecificObjective;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "specificObjectives", layout = MainView.class)
@ViewController("SpecificObjective.list")
@ViewDescriptor("specific-objective-list-view.xml")
@LookupComponent("specificObjectivesDataGrid")
@DialogMode(width = "64em")
public class SpecificObjectiveListView extends StandardListView<SpecificObjective> {
}