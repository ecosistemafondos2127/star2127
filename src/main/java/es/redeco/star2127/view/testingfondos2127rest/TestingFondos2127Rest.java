package es.redeco.star2127.view.testingfondos2127rest;


import com.vaadin.flow.component.ClickEvent;
import es.redeco.star2127.services.FondosNutsService;
import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.component.textarea.JmixTextArea;
import io.jmix.flowui.kit.component.button.JmixButton;
import io.jmix.flowui.view.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

@Route(value = "testing-Fondos2127-Rest", layout = MainView.class)
@ViewController("TestingFondos2127Rest")
@ViewDescriptor("testing-Fondos2127-Rest.xml")
public class TestingFondos2127Rest extends StandardView {
    @Autowired
    private FondosNutsService fondosNutsService;
    @ViewComponent
    private JmixTextArea authorizationBearer;
    @ViewComponent
    private JmixTextArea resultRestCall;
    @ViewComponent
    private JmixTextArea cookie;

    @Subscribe(id = "testRestCall", subject = "clickListener")
    public void onTestRestCallClick(final ClickEvent<JmixButton> event) {
        if (!authorizationBearer.getValue().isEmpty()) {
            String rst = fondosNutsService.loadAndSaveFondoss2127URL(authorizationBearer.getValue(), cookie.getValue());
            resultRestCall.setValue(rst);
        }
    }
}