package es.redeco.star2127.view.gender;

import es.redeco.star2127.entity.Gender;
import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "genders", layout = MainView.class)
@ViewController("Gender.list")
@ViewDescriptor("gender-list-view.xml")
@LookupComponent("gendersDataGrid")
@DialogMode(width = "64em")
public class GenderListView extends StandardListView<Gender> {
}