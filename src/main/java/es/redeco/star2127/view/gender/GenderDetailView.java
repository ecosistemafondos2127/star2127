package es.redeco.star2127.view.gender;

import es.redeco.star2127.entity.Gender;
import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "genders/:id", layout = MainView.class)
@ViewController("Gender.detail")
@ViewDescriptor("gender-detail-view.xml")
@EditedEntityContainer("genderDc")
public class GenderDetailView extends StandardDetailView<Gender> {
}