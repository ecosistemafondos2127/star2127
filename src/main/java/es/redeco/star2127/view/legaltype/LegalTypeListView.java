package es.redeco.star2127.view.legaltype;

import es.redeco.star2127.entity.LegalType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "legalTypes", layout = MainView.class)
@ViewController("LegalType.list")
@ViewDescriptor("legal-type-list-view.xml")
@LookupComponent("legalTypesDataGrid")
@DialogMode(width = "64em")
public class LegalTypeListView extends StandardListView<LegalType> {
}