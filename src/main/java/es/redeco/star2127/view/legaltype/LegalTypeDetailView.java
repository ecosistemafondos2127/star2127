package es.redeco.star2127.view.legaltype;

import es.redeco.star2127.entity.LegalType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "legalTypes/:id", layout = MainView.class)
@ViewController("LegalType.detail")
@ViewDescriptor("legal-type-detail-view.xml")
@EditedEntityContainer("legalTypeDc")
public class LegalTypeDetailView extends StandardDetailView<LegalType> {
}