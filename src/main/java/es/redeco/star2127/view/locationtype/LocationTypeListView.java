package es.redeco.star2127.view.locationtype;

import es.redeco.star2127.entity.LocationType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "locationTypes", layout = MainView.class)
@ViewController("LocationType.list")
@ViewDescriptor("location-type-list-view.xml")
@LookupComponent("locationTypesDataGrid")
@DialogMode(width = "64em")
public class LocationTypeListView extends StandardListView<LocationType> {
}