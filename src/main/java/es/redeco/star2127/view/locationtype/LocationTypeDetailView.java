package es.redeco.star2127.view.locationtype;

import es.redeco.star2127.entity.LocationType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "locationTypes/:id", layout = MainView.class)
@ViewController("LocationType.detail")
@ViewDescriptor("location-type-detail-view.xml")
@EditedEntityContainer("locationTypeDc")
public class LocationTypeDetailView extends StandardDetailView<LocationType> {
}