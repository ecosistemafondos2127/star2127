package es.redeco.star2127.view.legalentity;

import es.redeco.star2127.entity.LegalEntity;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "legalEntities/:id", layout = MainView.class)
@ViewController("LegalEntity.detail")
@ViewDescriptor("legal-entity-detail-view.xml")
@EditedEntityContainer("legalEntityDc")
public class LegalEntityDetailView extends StandardDetailView<LegalEntity> {
}