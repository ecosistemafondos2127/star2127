package es.redeco.star2127.view.legalentity;

import es.redeco.star2127.entity.LegalEntity;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "legalEntities", layout = MainView.class)
@ViewController("LegalEntity.list")
@ViewDescriptor("legal-entity-list-view.xml")
@LookupComponent("legalEntitiesDataGrid")
@DialogMode(width = "64em")
public class LegalEntityListView extends StandardListView<LegalEntity> {
}