package es.redeco.star2127.view.territorialfocus;

import es.redeco.star2127.entity.TerritorialFocus;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "territorialFocuses", layout = MainView.class)
@ViewController("TerritorialFocus.list")
@ViewDescriptor("territorial-focus-list-view.xml")
@LookupComponent("territorialFocusesDataGrid")
@DialogMode(width = "64em")
public class TerritorialFocusListView extends StandardListView<TerritorialFocus> {
}