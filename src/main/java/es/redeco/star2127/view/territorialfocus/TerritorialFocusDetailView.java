package es.redeco.star2127.view.territorialfocus;

import es.redeco.star2127.entity.TerritorialFocus;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "territorialFocuses/:id", layout = MainView.class)
@ViewController("TerritorialFocus.detail")
@ViewDescriptor("territorial-focus-detail-view.xml")
@EditedEntityContainer("territorialFocusDc")
public class TerritorialFocusDetailView extends StandardDetailView<TerritorialFocus> {
}