package es.redeco.star2127.view.dataconnectionconfigurations;

import es.redeco.star2127.entity.DataConnectionConfigurations;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "dataConnectionConfigurationses", layout = MainView.class)
@ViewController("DataConnectionConfigurations.list")
@ViewDescriptor("data-connection-configurations-list-view.xml")
@LookupComponent("dataConnectionConfigurationsesDataGrid")
@DialogMode(width = "64em")
public class DataConnectionConfigurationsListView extends StandardListView<DataConnectionConfigurations> {
}