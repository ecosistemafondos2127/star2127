package es.redeco.star2127.view.dataconnectionconfigurations;

import es.redeco.star2127.entity.DataConnectionConfigurations;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "dataConnectionConfigurationses/:id", layout = MainView.class)
@ViewController("DataConnectionConfigurations.detail")
@ViewDescriptor("data-connection-configurations-detail-view.xml")
@EditedEntityContainer("dataConnectionConfigurationsDc")
public class DataConnectionConfigurationsDetailView extends StandardDetailView<DataConnectionConfigurations> {
}