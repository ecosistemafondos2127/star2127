package es.redeco.star2127.view.bitemporalexample;

import es.redeco.star2127.entity.BitemporalExample;
import es.redeco.star2127.entity.EditionStatus;
import es.redeco.star2127.temporalversioning.BitemporalEntity;
import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.core.DataManager;
import io.jmix.flowui.app.inputdialog.DialogActions;
import io.jmix.flowui.app.inputdialog.DialogOutcome;
import io.jmix.flowui.app.inputdialog.InputParameter;
import io.jmix.flowui.component.grid.DataGrid;
import io.jmix.flowui.component.upload.FileStorageUploadField;
import io.jmix.flowui.kit.action.ActionPerformedEvent;
import io.jmix.flowui.model.DataComponents;
import io.jmix.flowui.model.DataContext;
import io.jmix.flowui.view.*;
import io.jmix.flowui.Dialogs;
import io.jmix.flowui.action.DialogAction;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.Optional;

import static io.jmix.flowui.app.inputdialog.InputParameter.booleanParameter;
import static io.jmix.flowui.app.inputdialog.InputParameter.localDateTimeParameter;

@Route(value = "bitemporalExamples/:id", layout = MainView.class)
@ViewController("BitemporalExample.detail")
@ViewDescriptor("bitemporal-example-detail-view.xml")
@EditedEntityContainer("bitemporalExampleDc")
public class BitemporalExampleDetailView extends StandardDetailView<BitemporalExample> {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(BitemporalExampleDetailView.class);
    @Autowired
    private DataManager dataManager;
    @Autowired
    private Dialogs dialogs;

    @Subscribe("saveAndCloseVersionAction")
    public void onSaveAndCloseVersionAction(final ActionPerformedEvent event) {
        log.debug("onWindowCloseVersionAndClose");
        DataContext dataContext = this.getViewData().getDataContextOrNull();
        BitemporalExample regNew = dataContext.merge(getEditedEntity());
        getEffectiveDates(regNew);
    }

    public void getEffectiveDates(BitemporalExample sec) {
        if (sec.getEditVersion() == EditionStatus.OPEN_MAJOR) {
            dialogs.createInputDialog(this)
                    .withHeader("Obtener Fechas de Validez")
                    .withParameters(
                            localDateTimeParameter("beginInstant")
                                    .withLabel("Fecha inicio")
                                    .withRequired(true),
                            booleanParameter("hasEndInstant")
                                    .withLabel("¿Existe fecha final?")
                                    .withRequired(true),
                            localDateTimeParameter("endInstant")
                                    .withLabel("Fecha final")
                                    .withRequired(false)
                    )
                    .withActions(DialogActions.OK_CANCEL)
                    .withCloseListener(closeEvent -> {
                        if (closeEvent.closedWith(DialogOutcome.OK)) {
                            LocalDateTime from = closeEvent.getValue("beginInstant");
                            Boolean hasEnd = closeEvent.getValue("hasEndInstant");
                            LocalDateTime to;
                            if (Boolean.TRUE.equals(hasEnd))
                                to = closeEvent.getValue("endInstant");
                            else
                                to = BitemporalEntity.INFINITE;
                            closeMajor(sec, from, hasEnd, to);
                            closeWithSave();
                        } else
                            closeWithDiscard();
                    })
                    .open();
        } else if (sec.getEditVersion() == EditionStatus.OPEN_MINOR) {
            dialogs.createOptionDialog()
                    .withHeader("Por favor, confirme")
                    .withText("¿Desea cerrar la versión?")
                    .withActions(
                            new DialogAction(DialogAction.Type.YES)
                                    .withHandler(e ->
                                    {
                                        closeMinor(sec);
                                        closeWithSave();
                                    }),
                            new DialogAction(DialogAction.Type.NO)
                    )
                    .open();
        }
    }

    private  void closeMajor(BitemporalExample datarec, LocalDateTime from, Boolean hasEnd, LocalDateTime to) {
        //TODO Check this query
        Optional<BitemporalExample> previous = dataManager.load(BitemporalExample.class)
                .query("e.someValue = ?1 and e.editVersion = 'Valid' order by e.vt_Start desc", datarec.getSomeValue())
                .optional();

        if (previous.isEmpty())
            log.error("No previous record found before close edition");

        // make sure we use the same "now" in both records
        LocalDateTime now = LocalDateTime.now().minusSeconds(30);

        // is there a previous version?
        if (previous.isPresent()) {
            BitemporalExample datarec2 = previous.get();
            datarec2.setEditVersion(EditionStatus.NOT_VALID);
            // Major Version sets both record and valid periods

            // Set Effective (validity) Period
            datarec2.setEffectivePeriod(datarec2.getVt_Start(), from);
            // Set Transaction (record) period
            datarec.setRecordPeriod(datarec2.getTt_Start(), now);

            dataManager.save(datarec2);
        }
        datarec.setEffectivePeriod(from, to);
        datarec.setRecordPeriod(now, BitemporalEntity.INFINITE);
        datarec.setEditVersion(EditionStatus.VALID);
    }

    private  void closeMinor(BitemporalExample datarec) {
        //TODO Check this query
        Optional<BitemporalExample> previous = dataManager.load(BitemporalExample.class)
                .query("e.someValue = ?1 and e.editVersion = 'Valid' order by e.vt_Start desc", datarec.getSomeValue())
                .optional();

        if (previous.isEmpty())
            log.error("No previous record found before close edition");

        // make sure we use the same "now" in both records
        LocalDateTime now = LocalDateTime.now().minusSeconds(30);

        // is there a previous version?
        if (previous.isPresent()) {
            // Minor Version only sets record period
            BitemporalExample datarec2 = previous.get();
            datarec2.setEditVersion(EditionStatus.NOT_VALID);

            // Set Transaction (record) period
            datarec2.setRecordPeriod(datarec2.getTt_Start(), now);
            dataManager.save(datarec2);
        }

        datarec.setRecordPeriod(now, BitemporalEntity.INFINITE);
        datarec.setEditVersion(EditionStatus.VALID);
    }
}