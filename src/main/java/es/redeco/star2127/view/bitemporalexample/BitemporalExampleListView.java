package es.redeco.star2127.view.bitemporalexample;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.notification.NotificationVariant;
import com.vaadin.flow.component.upload.Receiver;
import es.redeco.star2127.entity.BitemporalExample;

import es.redeco.star2127.entity.EditionStatus;
import es.redeco.star2127.entity.RecordState;
import es.redeco.star2127.temporalversioning.Version;
import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.core.DataManager;
import io.jmix.dataimport.DataImporter;
import io.jmix.dataimport.InputDataFormat;
import io.jmix.dataimport.configuration.DuplicateEntityPolicy;
import io.jmix.dataimport.configuration.ImportConfiguration;
import io.jmix.dataimport.configuration.ImportTransactionStrategy;
import io.jmix.dataimport.result.ImportResult;
import io.jmix.flowui.Dialogs;
import io.jmix.flowui.Notifications;
import io.jmix.flowui.action.DialogAction;
import io.jmix.core.FileRef;
import io.jmix.flowui.component.grid.DataGrid;
import io.jmix.flowui.component.upload.FileStorageUploadField;
import io.jmix.flowui.component.upload.receiver.FileTemporaryStorageBuffer;
import io.jmix.flowui.kit.action.ActionPerformedEvent;
import io.jmix.flowui.kit.component.button.JmixButton;
import io.jmix.flowui.kit.component.upload.event.FileUploadSucceededEvent;
import io.jmix.flowui.upload.TemporaryStorage;
import io.jmix.flowui.view.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.UUID;

@Route(value = "bitemporalExamples", layout = MainView.class)
@ViewController("BitemporalExample.list")
@ViewDescriptor("bitemporal-example-list-view.xml")
@LookupComponent("bitemporalExamplesDataGrid")
@DialogMode(width = "64em")
public class BitemporalExampleListView extends StandardListView<BitemporalExample> {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(BitemporalExampleListView.class);

    @Autowired
    private DataManager dataManager;
    @Autowired
    protected DataImporter dataImporter;
    @Autowired
    private Notifications notifications;
    @Autowired
    private Dialogs dialogs;
    @Autowired
    private TemporaryStorage temporaryStorage;

    @ViewComponent
    private DataGrid<BitemporalExample> bitemporalExamplesDataGrid;
    @ViewComponent
    private FileStorageUploadField fileStorageUploadField;



    //@Subscribe("bitemporalExamplesDataGrid.seeAllVersions")
    public void onBitemporalExamplesDataGridSeeAllVersions(final ActionPerformedEvent event) {
        log.debug("onBitemporalExamplesDataGridSeeAllVersions");
    }

    @Subscribe("bitemporalExamplesDataGrid.openEdition")
    public void onBitemporalExamplesDataGridOpenEdition(final ActionPerformedEvent event) {
        log.debug("onBitemporalExamplesDataGridOpenEdition");
        BitemporalExample aux = bitemporalExamplesDataGrid.getSingleSelectedItem();
        if (aux == null) return;
        if (aux.getEditVersion() != EditionStatus.VALID)
            return;

        dialogs.createOptionDialog()
                .withHeader("¿La edición altera la legalidad?")
                .withText("El cambio en la información puede modificar el periodo de validez. Selected: " + aux.getId())
                .withActions(
                        new DialogAction(DialogAction.Type.YES)
                                .withHandler(e -> doMajorChange(aux)),
                        new DialogAction(DialogAction.Type.NO)
                                .withHandler(e -> doMinorChange(aux)),
                        new DialogAction(DialogAction.Type.CANCEL)
                )
                .open();
    }

    @Subscribe(id = "editBtn", subject = "clickListener")
    public void onEditBtnClick(final ClickEvent<JmixButton> event) {
        log.debug("onEditBtnClick");
        BitemporalExample aux = bitemporalExamplesDataGrid.getSingleSelectedItem();
        if (aux != null && aux.getEditVersion() != EditionStatus.OPEN_MAJOR && aux.getEditVersion() != EditionStatus.OPEN_MINOR) {
            notifications.create("Solo puede editar si la versión está abierta")
                    .withType(Notifications.Type.ERROR)
                    .show();
            //TODO Prevent edit execution
        }
    }

    private void doMajorChange(BitemporalExample reg) {
        assert reg != null;
        BitemporalExample regNew = dataManager.create(BitemporalExample.class);
        BitemporalExample.copyRegs(reg, regNew);
        regNew.setEditVersion(EditionStatus.OPEN_MAJOR);
        Version ver = new Version(regNew.getVersionNumber());
        regNew.setVersionNumber(ver.getIncrementedMajorVersion());
        dataManager.save(regNew);
    }

    private void doMinorChange(BitemporalExample reg) {
        assert reg != null;
        BitemporalExample regNew = dataManager.create(BitemporalExample.class);
        BitemporalExample.copyRegs(reg, regNew);
        regNew.setEditVersion(EditionStatus.OPEN_MINOR);
        Version ver = new Version(regNew.getVersionNumber());
        regNew.setVersionNumber(ver.getIncrementedMinorVersion());
        dataManager.save(reg);
    }

    @Subscribe("fileStorageUploadField")
    public void onFileStorageUploadFieldFileUploadSucceeded(final FileUploadSucceededEvent<FileStorageUploadField> event) {
        log.debug("onFileStorageUploadFieldFileUploadSucceeded");
        Receiver receiver = event.getReceiver();
        if (receiver instanceof FileTemporaryStorageBuffer storageBuffer) {
            UUID fileId = storageBuffer.getFileData().getFileInfo().getId();
            File file = temporaryStorage.getFile(fileId);
            if (file != null) {
                FileRef fileRef = new FileRef("tempStorage", file.getAbsolutePath(), file.getName());
                fileStorageUploadField.setValue(fileRef);
                ImportResult rst = importCSV(event.getFileName());
                if (rst != null && rst.isSuccess())
                    notifications.create("Your file %s has been uploaded successfully.".formatted(event.getFileName()))
                        .withThemeVariant(NotificationVariant.LUMO_PRIMARY)
                        .show();
                else
                    notifications.create("Error loading file %s.".formatted(event.getFileName()))
                        .withThemeVariant(NotificationVariant.LUMO_ERROR)
                        .show();
                // Remove the uploaded file.
                // In a real-world application you would move the file to FileStorage here using
                // the temporaryStorage.putFileIntoStorage() method.
                temporaryStorage.deleteFile(fileId);
            }
        }
    }

    public ImportResult importCSV(final String filename) {
        log.debug("called importCSV");

        // more info at https://github.com/jmix-framework/jmix/tree/master/jmix-dataimport
        ImportConfiguration importConfiguration = ImportConfiguration.builder(BitemporalExample.class, InputDataFormat.CSV)
                .addSimplePropertyMapping("someValue", "UnValor")
                .addSimplePropertyMapping("description", "Descripcion")
                .addUniqueEntityConfiguration(DuplicateEntityPolicy.UPDATE, "someValue")
                .withDateFormat("dd/MM/yyyy HH:mm")
                .withTransactionStrategy(ImportTransactionStrategy.TRANSACTION_PER_ENTITY)
                .withEntityInitializer(entity -> {
                    BitemporalExample record = (BitemporalExample) entity;
                    record.setRecordStatus(RecordState.UNKNOWN);
                    record.setVersionNumber("1.0");
                    record.setEditVersion(EditionStatus.VALID);
                    record.setVt_Start(LocalDateTime.now());
                    record.setVt_End(LocalDateTime.of(9999, 12, 31,23, 59));
                    record.setTt_Start(LocalDateTime.now().minusMinutes(1));
                    record.setTt_End(LocalDateTime.of(9999, 12, 31,23, 59));
                })
                .build();

        File initialFile = new File(filename);
        String userDirectory = initialFile.getAbsolutePath();
        try {
            InputStream inputStream = new FileInputStream(initialFile);

            ImportResult result = dataImporter.importData(importConfiguration, inputStream);
            log.debug("Import result: " + result);
            return  result;
        } catch (FileNotFoundException e) {
            log.debug("File to import not found: " + userDirectory);
            return null;
        }
    }
}