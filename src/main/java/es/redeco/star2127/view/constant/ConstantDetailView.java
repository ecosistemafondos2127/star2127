package es.redeco.star2127.view.constant;

import es.redeco.star2127.entity.Constant;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "constants/:id", layout = MainView.class)
@ViewController("Constant.detail")
@ViewDescriptor("constant-detail-view.xml")
@EditedEntityContainer("constantDc")
public class ConstantDetailView extends StandardDetailView<Constant> {
}