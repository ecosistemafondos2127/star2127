package es.redeco.star2127.view.constant;

import es.redeco.star2127.entity.Constant;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "constants", layout = MainView.class)
@ViewController("Constant.list")
@ViewDescriptor("constant-list-view.xml")
@LookupComponent("constantsDataGrid")
@DialogMode(width = "64em")
public class ConstantListView extends StandardListView<Constant> {
}