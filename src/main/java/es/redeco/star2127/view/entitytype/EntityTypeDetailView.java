package es.redeco.star2127.view.entitytype;

import es.redeco.star2127.entity.EntityType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "entityTypes/:id", layout = MainView.class)
@ViewController("EntityType.detail")
@ViewDescriptor("entity-type-detail-view.xml")
@EditedEntityContainer("entityTypeDc")
public class EntityTypeDetailView extends StandardDetailView<EntityType> {
}