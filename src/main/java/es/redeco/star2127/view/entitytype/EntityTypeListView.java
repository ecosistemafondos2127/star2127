package es.redeco.star2127.view.entitytype;

import es.redeco.star2127.entity.EntityType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "entityTypes", layout = MainView.class)
@ViewController("EntityType.list")
@ViewDescriptor("entity-type-list-view.xml")
@LookupComponent("entityTypesDataGrid")
@DialogMode(width = "64em")
public class EntityTypeListView extends StandardListView<EntityType> {
}