package es.redeco.star2127.view.genderequality;

import es.redeco.star2127.entity.GenderEquality;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "genderEqualities", layout = MainView.class)
@ViewController("GenderEquality.list")
@ViewDescriptor("gender-equality-list-view.xml")
@LookupComponent("genderEqualitiesDataGrid")
@DialogMode(width = "64em")
public class GenderEqualityListView extends StandardListView<GenderEquality> {
}