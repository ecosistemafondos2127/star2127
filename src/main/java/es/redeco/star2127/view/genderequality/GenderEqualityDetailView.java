package es.redeco.star2127.view.genderequality;

import es.redeco.star2127.entity.GenderEquality;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "genderEqualities/:id", layout = MainView.class)
@ViewController("GenderEquality.detail")
@ViewDescriptor("gender-equality-detail-view.xml")
@EditedEntityContainer("genderEqualityDc")
public class GenderEqualityDetailView extends StandardDetailView<GenderEquality> {
}