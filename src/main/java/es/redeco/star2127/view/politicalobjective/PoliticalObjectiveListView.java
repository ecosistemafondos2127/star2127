package es.redeco.star2127.view.politicalobjective;

import es.redeco.star2127.entity.PoliticalObjective;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "politicalObjectives", layout = MainView.class)
@ViewController("PoliticalObjective.list")
@ViewDescriptor("political-objective-list-view.xml")
@LookupComponent("politicalObjectivesDataGrid")
@DialogMode(width = "64em")
public class PoliticalObjectiveListView extends StandardListView<PoliticalObjective> {
}