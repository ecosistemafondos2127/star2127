package es.redeco.star2127.view.politicalobjective;

import es.redeco.star2127.entity.PoliticalObjective;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "politicalObjectives/:id", layout = MainView.class)
@ViewController("PoliticalObjective.detail")
@ViewDescriptor("political-objective-detail-view.xml")
@EditedEntityContainer("politicalObjectiveDc")
public class PoliticalObjectiveDetailView extends StandardDetailView<PoliticalObjective> {
}