package es.redeco.star2127.view.categoryofregion;

import es.redeco.star2127.entity.CategoryOfRegion;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "categoryOfRegions/:id", layout = MainView.class)
@ViewController("CategoryOfRegion.detail")
@ViewDescriptor("category-of-region-detail-view.xml")
@EditedEntityContainer("categoryOfRegionDc")
public class CategoryOfRegionDetailView extends StandardDetailView<CategoryOfRegion> {
}