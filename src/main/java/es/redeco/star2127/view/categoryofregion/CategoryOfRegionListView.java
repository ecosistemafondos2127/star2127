package es.redeco.star2127.view.categoryofregion;

import es.redeco.star2127.entity.CategoryOfRegion;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "categoryOfRegions", layout = MainView.class)
@ViewController("CategoryOfRegion.list")
@ViewDescriptor("category-of-region-list-view.xml")
@LookupComponent("categoryOfRegionsDataGrid")
@DialogMode(width = "64em")
public class CategoryOfRegionListView extends StandardListView<CategoryOfRegion> {
}