package es.redeco.star2127.view.formofsupport;

import es.redeco.star2127.entity.FormOfSupport;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "formOfSupports", layout = MainView.class)
@ViewController("FormOfSupport.list")
@ViewDescriptor("form-of-support-list-view.xml")
@LookupComponent("formOfSupportsDataGrid")
@DialogMode(width = "64em")
public class FormOfSupportListView extends StandardListView<FormOfSupport> {
}