package es.redeco.star2127.view.formofsupport;

import es.redeco.star2127.entity.FormOfSupport;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "formOfSupports/:id", layout = MainView.class)
@ViewController("FormOfSupport.detail")
@ViewDescriptor("form-of-support-detail-view.xml")
@EditedEntityContainer("formOfSupportDc")
public class FormOfSupportDetailView extends StandardDetailView<FormOfSupport> {
}