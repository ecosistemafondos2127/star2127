package es.redeco.star2127.view.macroregionalseabasinstrategies;

import es.redeco.star2127.entity.MacroRegionalSeaBasinStrategies;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "macroRegionalSeaBasinStrategieses", layout = MainView.class)
@ViewController("MacroRegionalSeaBasinStrategies.list")
@ViewDescriptor("macro-regional-sea-basin-strategies-list-view.xml")
@LookupComponent("macroRegionalSeaBasinStrategiesesDataGrid")
@DialogMode(width = "64em")
public class MacroRegionalSeaBasinStrategiesListView extends StandardListView<MacroRegionalSeaBasinStrategies> {
}