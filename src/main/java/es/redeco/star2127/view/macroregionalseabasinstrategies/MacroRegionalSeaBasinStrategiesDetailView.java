package es.redeco.star2127.view.macroregionalseabasinstrategies;

import es.redeco.star2127.entity.MacroRegionalSeaBasinStrategies;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "macroRegionalSeaBasinStrategieses/:id", layout = MainView.class)
@ViewController("MacroRegionalSeaBasinStrategies.detail")
@ViewDescriptor("macro-regional-sea-basin-strategies-detail-view.xml")
@EditedEntityContainer("macroRegionalSeaBasinStrategiesDc")
public class MacroRegionalSeaBasinStrategiesDetailView extends StandardDetailView<MacroRegionalSeaBasinStrategies> {
}