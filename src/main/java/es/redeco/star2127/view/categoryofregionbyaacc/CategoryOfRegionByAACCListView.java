package es.redeco.star2127.view.categoryofregionbyaacc;

import es.redeco.star2127.entity.CategoryOfRegionByAACC;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "categoryOfRegionByAACCs", layout = MainView.class)
@ViewController("CategoryOfRegionByAACC.list")
@ViewDescriptor("category-of-region-by-aacc-list-view.xml")
@LookupComponent("categoryOfRegionByAACCsDataGrid")
@DialogMode(width = "64em")
public class CategoryOfRegionByAACCListView extends StandardListView<CategoryOfRegionByAACC> {
}