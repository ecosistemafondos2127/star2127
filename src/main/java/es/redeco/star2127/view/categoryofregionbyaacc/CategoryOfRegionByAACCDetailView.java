package es.redeco.star2127.view.categoryofregionbyaacc;

import es.redeco.star2127.entity.CategoryOfRegionByAACC;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "categoryOfRegionByAACCs/:id", layout = MainView.class)
@ViewController("CategoryOfRegionByAACC.detail")
@ViewDescriptor("category-of-region-by-aacc-detail-view.xml")
@EditedEntityContainer("categoryOfRegionByAACCDc")
public class CategoryOfRegionByAACCDetailView extends StandardDetailView<CategoryOfRegionByAACC> {
}