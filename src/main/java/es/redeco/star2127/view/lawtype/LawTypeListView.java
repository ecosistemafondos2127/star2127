package es.redeco.star2127.view.lawtype;

import es.redeco.star2127.entity.LawType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "lawTypes", layout = MainView.class)
@ViewController("LawType.list")
@ViewDescriptor("law-type-list-view.xml")
@LookupComponent("lawTypesDataGrid")
@DialogMode(width = "64em")
public class LawTypeListView extends StandardListView<LawType> {
}