package es.redeco.star2127.view.lawtype;

import es.redeco.star2127.entity.LawType;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "lawTypes/:id", layout = MainView.class)
@ViewController("LawType.detail")
@ViewDescriptor("law-type-detail-view.xml")
@EditedEntityContainer("lawTypeDc")
public class LawTypeDetailView extends StandardDetailView<LawType> {
}