package es.redeco.star2127.view.group;

import es.redeco.star2127.entity.Groups;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "groups/:id", layout = MainView.class)
@ViewController("Group.detail")
@ViewDescriptor("group-detail-view.xml")
@EditedEntityContainer("groupDc")
public class GroupDetailView extends StandardDetailView<Groups> {
}