package es.redeco.star2127.view.group;

import es.redeco.star2127.entity.Groups;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "Groups", layout = MainView.class)
@ViewController("Group.list")
@ViewDescriptor("group-list-view.xml")
@LookupComponent("groupsDataGrid")
@DialogMode(width = "64em")
public class GroupListView extends StandardListView<Groups> {
}