package es.redeco.star2127.view.fund;

import es.redeco.star2127.entity.Fund;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "funds/:id", layout = MainView.class)
@ViewController("Fund.detail")
@ViewDescriptor("fund-detail-view.xml")
@EditedEntityContainer("fundDc")
public class FundDetailView extends StandardDetailView<Fund> {
}