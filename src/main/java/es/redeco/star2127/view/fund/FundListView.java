package es.redeco.star2127.view.fund;

import es.redeco.star2127.entity.Fund;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "funds", layout = MainView.class)
@ViewController("Fund.list")
@ViewDescriptor("fund-list-view.xml")
@LookupComponent("fundsDataGrid")
@DialogMode(width = "64em")
public class FundListView extends StandardListView<Fund> {
}