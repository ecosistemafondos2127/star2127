package es.redeco.star2127.view.interventionfield;

import es.redeco.star2127.entity.InterventionField;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "interventionFields", layout = MainView.class)
@ViewController("InterventionField.list")
@ViewDescriptor("intervention-field-list-view.xml")
@LookupComponent("interventionFieldsDataGrid")
@DialogMode(width = "64em")
public class InterventionFieldListView extends StandardListView<InterventionField> {
}