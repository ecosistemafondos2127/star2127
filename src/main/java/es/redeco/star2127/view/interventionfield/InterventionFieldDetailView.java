package es.redeco.star2127.view.interventionfield;

import es.redeco.star2127.entity.InterventionField;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "interventionFields/:id", layout = MainView.class)
@ViewController("InterventionField.detail")
@ViewDescriptor("intervention-field-detail-view.xml")
@EditedEntityContainer("interventionFieldDc")
public class InterventionFieldDetailView extends StandardDetailView<InterventionField> {
}