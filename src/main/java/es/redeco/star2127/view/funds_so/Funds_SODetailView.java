package es.redeco.star2127.view.funds_so;

import es.redeco.star2127.entity.Funds_SO;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "funds_Soes/:id", layout = MainView.class)
@ViewController("Funds_SO.detail")
@ViewDescriptor("funds_so-detail-view.xml")
@EditedEntityContainer("funds_SODc")
public class Funds_SODetailView extends StandardDetailView<Funds_SO> {
}