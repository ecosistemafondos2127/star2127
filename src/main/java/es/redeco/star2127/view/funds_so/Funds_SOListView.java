package es.redeco.star2127.view.funds_so;

import es.redeco.star2127.entity.Funds_SO;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "funds_Soes", layout = MainView.class)
@ViewController("Funds_SO.list")
@ViewDescriptor("funds_so-list-view.xml")
@LookupComponent("funds_SoesDataGrid")
@DialogMode(width = "64em")
public class Funds_SOListView extends StandardListView<Funds_SO> {
}