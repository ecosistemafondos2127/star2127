package es.redeco.star2127.view.indicator;

import es.redeco.star2127.entity.Indicator;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "indicators/:id", layout = MainView.class)
@ViewController("Indicator_.detail")
@ViewDescriptor("indicator-detail-view.xml")
@EditedEntityContainer("indicatorDc")
public class IndicatorDetailView extends StandardDetailView<Indicator> {
}