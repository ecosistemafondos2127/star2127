package es.redeco.star2127.view.indicator;

import es.redeco.star2127.entity.Indicator;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "indicators", layout = MainView.class)
@ViewController("Indicator_.list")
@ViewDescriptor("indicator-list-view.xml")
@LookupComponent("indicatorsDataGrid")
@DialogMode(width = "64em")
public class IndicatorListView extends StandardListView<Indicator> {
}