package es.redeco.star2127.view.statedir3;

import es.redeco.star2127.entity.StateDIR3;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "stateDIR3s", layout = MainView.class)
@ViewController("StateDIR3.list")
@ViewDescriptor("state-dir3-list-view.xml")
@LookupComponent("stateDIR3sDataGrid")
@DialogMode(width = "64em")
public class StateDIR3ListView extends StandardListView<StateDIR3> {
}