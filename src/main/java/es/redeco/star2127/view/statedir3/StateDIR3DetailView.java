package es.redeco.star2127.view.statedir3;

import es.redeco.star2127.entity.StateDIR3;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "stateDIR3s/:id", layout = MainView.class)
@ViewController("StateDIR3.detail")
@ViewDescriptor("state-dir3-detail-view.xml")
@EditedEntityContainer("stateDIR3Dc")
public class StateDIR3DetailView extends StandardDetailView<StateDIR3> {
}