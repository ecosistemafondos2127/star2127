package es.redeco.star2127.view.province;

import es.redeco.star2127.entity.Province;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "provinces/:id", layout = MainView.class)
@ViewController("Province.detail")
@ViewDescriptor("province-detail-view.xml")
@EditedEntityContainer("provinceDc")
public class ProvinceDetailView extends StandardDetailView<Province> {
}