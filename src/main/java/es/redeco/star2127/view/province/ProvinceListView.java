package es.redeco.star2127.view.province;

import es.redeco.star2127.entity.Province;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "provinces", layout = MainView.class)
@ViewController("Province.list")
@ViewDescriptor("province-list-view.xml")
@LookupComponent("provincesDataGrid")
@DialogMode(width = "64em")
public class ProvinceListView extends StandardListView<Province> {
}