package es.redeco.star2127.view.cnclassification;

import es.redeco.star2127.entity.CNClassification;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "cNClassifications", layout = MainView.class)
@ViewController("CNClassification.list")
@ViewDescriptor("cn-classification-list-view.xml")
@LookupComponent("cNClassificationsDataGrid")
@DialogMode(width = "64em")
public class CNClassificationListView extends StandardListView<CNClassification> {
}