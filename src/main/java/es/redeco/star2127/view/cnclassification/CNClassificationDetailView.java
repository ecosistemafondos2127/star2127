package es.redeco.star2127.view.cnclassification;

import es.redeco.star2127.entity.CNClassification;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "cNClassifications/:id", layout = MainView.class)
@ViewController("CNClassification.detail")
@ViewDescriptor("cn-classification-detail-view.xml")
@EditedEntityContainer("cNClassificationDc")
public class CNClassificationDetailView extends StandardDetailView<CNClassification> {
}