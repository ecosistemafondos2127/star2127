package es.redeco.star2127.view.administrativelevel;

import es.redeco.star2127.entity.AdministrativeLevel;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "administrativeLevels/:id", layout = MainView.class)
@ViewController("AdministrativeLevel.detail")
@ViewDescriptor("administrative-level-detail-view.xml")
@EditedEntityContainer("administrativeLevelDc")
public class AdministrativeLevelDetailView extends StandardDetailView<AdministrativeLevel> {
}