package es.redeco.star2127.view.administrativelevel;

import es.redeco.star2127.entity.AdministrativeLevel;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "administrativeLevels", layout = MainView.class)
@ViewController("AdministrativeLevel.list")
@ViewDescriptor("administrative-level-list-view.xml")
@LookupComponent("administrativeLevelsDataGrid")
@DialogMode(width = "64em")
public class AdministrativeLevelListView extends StandardListView<AdministrativeLevel> {
}