package es.redeco.star2127.view.postalcode;

import es.redeco.star2127.entity.PostalCode;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "postalCodes", layout = MainView.class)
@ViewController("PostalCode.list")
@ViewDescriptor("postal-code-list-view.xml")
@LookupComponent("postalCodesDataGrid")
@DialogMode(width = "64em")
public class PostalCodeListView extends StandardListView<PostalCode> {
}