package es.redeco.star2127.view.postalcode;

import es.redeco.star2127.entity.PostalCode;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "postalCodes/:id", layout = MainView.class)
@ViewController("PostalCode.detail")
@ViewDescriptor("postal-code-detail-view.xml")
@EditedEntityContainer("postalCodeDc")
public class PostalCodeDetailView extends StandardDetailView<PostalCode> {
}