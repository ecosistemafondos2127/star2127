package es.redeco.star2127.view.autonomouscommunity;

import es.redeco.star2127.entity.AutonomousCommunity;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "autonomousCommunities", layout = MainView.class)
@ViewController("AutonomousCommunity.list")
@ViewDescriptor("autonomous-community-list-view.xml")
@LookupComponent("autonomousCommunitiesDataGrid")
@DialogMode(width = "64em")
public class AutonomousCommunityListView extends StandardListView<AutonomousCommunity> {
}