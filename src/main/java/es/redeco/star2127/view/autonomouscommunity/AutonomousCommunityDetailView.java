package es.redeco.star2127.view.autonomouscommunity;

import es.redeco.star2127.entity.AutonomousCommunity;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "autonomousCommunities/:id", layout = MainView.class)
@ViewController("AutonomousCommunity.detail")
@ViewDescriptor("autonomous-community-detail-view.xml")
@EditedEntityContainer("autonomousCommunityDc")
public class AutonomousCommunityDetailView extends StandardDetailView<AutonomousCommunity> {
}