package es.redeco.star2127.view.municipality;

import es.redeco.star2127.entity.Municipality;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "municipalities", layout = MainView.class)
@ViewController("Municipality.list")
@ViewDescriptor("municipality-list-view.xml")
@LookupComponent("municipalitiesDataGrid")
@DialogMode(width = "64em")
public class MunicipalityListView extends StandardListView<Municipality> {
}