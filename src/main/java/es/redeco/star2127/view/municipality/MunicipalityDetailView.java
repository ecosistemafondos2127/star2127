package es.redeco.star2127.view.municipality;

import es.redeco.star2127.entity.Municipality;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "municipalities/:id", layout = MainView.class)
@ViewController("Municipality.detail")
@ViewDescriptor("municipality-detail-view.xml")
@EditedEntityContainer("municipalityDc")
public class MunicipalityDetailView extends StandardDetailView<Municipality> {
}