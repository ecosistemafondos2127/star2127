package es.redeco.star2127.view.country;

import es.redeco.star2127.entity.Country;

import es.redeco.star2127.view.main.MainView;

import com.vaadin.flow.router.Route;
import io.jmix.flowui.view.*;

@Route(value = "countries", layout = MainView.class)
@ViewController("Country.list")
@ViewDescriptor("country-list-view.xml")
@LookupComponent("countriesDataGrid")
@DialogMode(width = "64em")
public class CountryListView extends StandardListView<Country> {
}