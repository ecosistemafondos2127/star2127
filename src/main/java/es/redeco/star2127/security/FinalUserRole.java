package es.redeco.star2127.security;

import es.redeco.star2127.entity.*;
import io.jmix.security.model.EntityAttributePolicyAction;
import io.jmix.security.model.EntityPolicyAction;
import io.jmix.security.role.annotation.EntityAttributePolicy;
import io.jmix.security.role.annotation.EntityPolicy;
import io.jmix.security.role.annotation.ResourceRole;
import io.jmix.securityflowui.role.annotation.MenuPolicy;
import io.jmix.securityflowui.role.annotation.ViewPolicy;

@ResourceRole(name = "FinalUserRole", code = FinalUserRole.CODE)
public interface FinalUserRole {
    String CODE = "final-user-role";

    @EntityPolicy(entityClass = AdministrativeLevel.class, actions = EntityPolicyAction.READ)
    @EntityAttributePolicy(entityClass = AdministrativeLevel.class, attributes = {"id", "funds2127id", "code", "nameES"}, action = EntityAttributePolicyAction.VIEW)
    void administrativeLevel();

    @EntityAttributePolicy(entityClass = AutonomousCommunity.class, attributes = {"id", "isoCode31662", "funds2127id", "acronymES", "nameES", "nameEN", "country", "codeNuts2"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = AutonomousCommunity.class, actions = EntityPolicyAction.READ)
    void autonomousCommunity();

    @EntityPolicy(entityClass = BitemporalExample.class, actions = EntityPolicyAction.READ)
    @EntityAttributePolicy(entityClass = BitemporalExample.class, attributes = {"id", "recordStatus", "editVersion", "vt_Start", "vt_End", "tt_Start", "tt_End", "someValue", "description"}, action = EntityAttributePolicyAction.VIEW)
    void bitemporalExample();

    @EntityAttributePolicy(entityClass = CNClassification.class, attributes = {"id", "code", "nameES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = CNClassification.class, actions = EntityPolicyAction.READ)
    void cNClassification();

    @EntityAttributePolicy(entityClass = CategoryOfRegion.class, attributes = {"id", "codeES", "codeEN", "descriptionES", "descriptionEN", "nameES", "shortNameES", "nameEN", "shortNameEN", "order"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = CategoryOfRegion.class, actions = EntityPolicyAction.READ)
    void categoryOfRegion();

    @EntityAttributePolicy(entityClass = CategoryOfRegionByAACC.class, attributes = {"id", "codeNutII", "codeNut2", "codeRegionCat", "descriptionES", "nameRegionCat", "nameNutII", "percentage"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = CategoryOfRegionByAACC.class, actions = EntityPolicyAction.READ)
    void categoryOfRegionByAACC();

    @EntityAttributePolicy(entityClass = Country.class, attributes = {"id", "funds2127id", "nameES", "shortNameES", "nameEN", "shortNameEN", "codeNUTS1", "isoCodeAlpha2", "isoCodeAlpha3", "isEU"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Country.class, actions = EntityPolicyAction.READ)
    void country();

    @EntityAttributePolicy(entityClass = Constant.class, attributes = {"id", "rvDomain", "rvLowValue", "rvHighValue", "rvAbbreviation", "rvMeaning"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Constant.class, actions = EntityPolicyAction.READ)
    void constant();

    @EntityAttributePolicy(entityClass = EconomicActivity.class, attributes = {"id", "code", "codeSfc", "nameES", "nameEN", "descriptionES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = EconomicActivity.class, actions = EntityPolicyAction.READ)
    void economicActivity();

    @EntityAttributePolicy(entityClass = EnablingConditions.class, attributes = {"id", "code", "codeSfc", "nameES", "conditionType"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = EnablingConditions.class, actions = EntityPolicyAction.READ)
    void enablingConditions();

    @EntityPolicy(entityClass = Validation.class, actions = EntityPolicyAction.READ)
    @EntityAttributePolicy(entityClass = Validation.class, attributes = {"id", "recordStatus", "editVersion", "vt_Start", "vt_End", "tt_Start", "tt_End", "code", "level", "description", "message", "order", "active", "automatic", "logicLanguaje", "logicProcessable", "processContinous", "languageLogicBack", "logicProcessableBack", "languageLogicFront", "logicProcessableFront", "idFundGroup", "fundCode", "descrFund", "codeGroup", "descrGroup", "idEntityType", "codeEntityType", "state", "originVersion", "canEdit", "funds2127Id"}, action = EntityAttributePolicyAction.VIEW)
    void validation();

    @EntityPolicy(entityClass = User.class, actions = EntityPolicyAction.READ)
    @EntityAttributePolicy(entityClass = User.class, attributes = "*", action = EntityAttributePolicyAction.VIEW)
    void user();

    @EntityAttributePolicy(entityClass = TerritorialFocus.class, attributes = {"id", "nameES", "codeApproach", "codeMechanism"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = TerritorialFocus.class, actions = EntityPolicyAction.READ)
    void territorialFocus();

    @EntityAttributePolicy(entityClass = StateDIR3.class, attributes = {"id", "code", "nameES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = StateDIR3.class, actions = EntityPolicyAction.READ)
    void stateDIR3();

    @EntityAttributePolicy(entityClass = StandardBitemporalEntity.class, attributes = {"id", "recordStatus", "editVersion", "vt_Start", "vt_End", "tt_Start", "tt_End"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = StandardBitemporalEntity.class, actions = EntityPolicyAction.READ)
    void standardBitemporalEntity();

    @EntityAttributePolicy(entityClass = Province.class, attributes = {"id", "funds2127id", "externalCode", "nameES", "nameEN", "isoCode31662", "codeNuts3", "autoComm"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Province.class, actions = EntityPolicyAction.READ)
    void province();

    @EntityAttributePolicy(entityClass = SpecificObjective.class, attributes = {"id", "policyObjective", "acronymES", "acronymEN", "code", "codeSfc", "descriptionES", "descriptionEN", "descriptionFR", "descriptionPT", "shortNameES", "shortNameEN", "shortNameFR", "shortNamePT", "nameES", "nameEN", "namePT", "nameFR", "isTa"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = SpecificObjective.class, actions = EntityPolicyAction.READ)
    void specificObjective();

    @EntityAttributePolicy(entityClass = PostalCode.class, attributes = {"id", "codeMunicipality", "nameMunicipality", "postalCode", "codeNUT3"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = PostalCode.class, actions = EntityPolicyAction.READ)
    void postalCode();

    @EntityAttributePolicy(entityClass = PoliticalObjective.class, attributes = {"id", "acronymES", "acronymEN", "code", "codeSfc", "descriptionES", "descriptionEN", "descriptionFR", "descriptionPT", "shortNameES", "shortNameEN", "shortNameFR", "shortNamePT", "nameES", "nameEN", "nameFR", "namePT", "isTa"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = PoliticalObjective.class, actions = EntityPolicyAction.READ)
    void politicalObjective();

    @EntityAttributePolicy(entityClass = Nuts.class, attributes = {"id", "funds2127id", "codeNuts", "nutsLevel", "nameES", "nameEN"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Nuts.class, actions = EntityPolicyAction.READ)
    void nuts();

    @EntityAttributePolicy(entityClass = NIFType.class, attributes = {"id", "code", "nameES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = NIFType.class, actions = EntityPolicyAction.READ)
    void nIFType();

    @EntityAttributePolicy(entityClass = Municipality.class, attributes = {"id", "funds2127id", "externalCode", "nameES", "nameEN", "codeINE", "codeNuts3", "province"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Municipality.class, actions = EntityPolicyAction.READ)
    void municipality();

    @EntityAttributePolicy(entityClass = MacroRegionalSeaBasinStrategies.class, attributes = {"id", "code", "codeSfc", "nameES", "nameEN", "descriptionES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = MacroRegionalSeaBasinStrategies.class, actions = EntityPolicyAction.READ)
    void macroRegionalSeaBasinStrategies();

    @EntityAttributePolicy(entityClass = LocationType.class, attributes = {"id", "code", "nameES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = LocationType.class, actions = EntityPolicyAction.READ)
    void locationType();

    @EntityAttributePolicy(entityClass = LegalType.class, attributes = {"id", "code", "nameES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = LegalType.class, actions = EntityPolicyAction.READ)
    void legalType();

    @EntityAttributePolicy(entityClass = LawType.class, attributes = {"id", "code", "nameES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = LawType.class, actions = EntityPolicyAction.READ)
    void lawType();

    @EntityAttributePolicy(entityClass = InterventionField.class, attributes = {"id", "code", "denomination", "percentageClimate", "percentageEnvironment", "allPoliticalObjectives", "asignedPoliticalObjective"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = InterventionField.class, actions = EntityPolicyAction.READ)
    void interventionField();

    @EntityAttributePolicy(entityClass = Groups.class, attributes = {"id", "funds2127Id", "code", "acronymEN", "nameES", "nameEN", "descriptionES", "descriptionEN", "ftGroupSO"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Groups.class, actions = EntityPolicyAction.READ)
    void group();

    @EntityAttributePolicy(entityClass = Gender.class, attributes = {"id", "funds2127id", "codeSFC", "nameES", "nameEN", "description"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Gender.class, actions = EntityPolicyAction.READ)
    void gender();

    @EntityAttributePolicy(entityClass = GenderEquality.class, attributes = {"id", "code", "nameES", "coefficient"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = GenderEquality.class, actions = EntityPolicyAction.READ)
    void genderEquality();

    @EntityAttributePolicy(entityClass = FundGroup.class, attributes = {"id", "code", "acronymEN", "nameES", "nameEN", "descriptionES", "descriptionEN"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = FundGroup.class, actions = EntityPolicyAction.READ)
    void fundGroup();

    @EntityAttributePolicy(entityClass = Funds_SO.class, attributes = {"id", "fund", "fundGroup"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Funds_SO.class, actions = EntityPolicyAction.READ)
    void funds_SO();

    @EntityAttributePolicy(entityClass = Fund.class, attributes = {"id", "acronymES", "acronymEN", "acronymFR", "acronymPT", "code", "nameES", "nameEN", "nameFR", "namePT", "descriptionES", "descriptionEN", "descriptionFR", "descriptionPT", "isCDR"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = Fund.class, actions = EntityPolicyAction.READ)
    void fund();

    @EntityAttributePolicy(entityClass = FormOfSupport.class, attributes = {"id", "code", "nameES"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = FormOfSupport.class, actions = EntityPolicyAction.READ)
    void formOfSupport();

    @EntityAttributePolicy(entityClass = EntityType.class, attributes = {"id", "funds2127id", "code", "tableName"}, action = EntityAttributePolicyAction.VIEW)
    @EntityPolicy(entityClass = EntityType.class, actions = EntityPolicyAction.READ)
    void entityType();

    @MenuPolicy(menuIds = {"EnablingConditions.list", "LawType.list", "NIFType.list", "LegalType.list", "AdministrativeLevel.list", "StateDIR3.list", "CNClassification.list", "Country.list", "AutonomousCommunity.list", "Municipality.list", "Nuts.list", "PostalCode.list", "Province.list", "Gender.list", "InterventionField.list", "FormOfSupport.list", "TerritorialFocus.list", "GenderEquality.list", "EconomicActivity.list", "MacroRegionalSeaBasinStrategies.list", "CategoryOfRegion.list", "Group.list", "Funds_SO.list", "Fund.list", "FundGroup.list", "PoliticalObjective.list", "SpecificObjective.list", "EntityType.list", "LocationType.list"})
    @ViewPolicy(viewIds = {"EnablingConditions.list", "LawType.list", "NIFType.list", "LegalType.list", "AdministrativeLevel.list", "StateDIR3.list", "CNClassification.list", "Country.list", "AutonomousCommunity.list", "Municipality.list", "Nuts.list", "PostalCode.list", "Province.list", "Gender.list", "InterventionField.list", "FormOfSupport.list", "TerritorialFocus.list", "GenderEquality.list", "EconomicActivity.list", "MacroRegionalSeaBasinStrategies.list", "CategoryOfRegion.list", "Group.list", "Funds_SO.list", "Fund.list", "FundGroup.list", "PoliticalObjective.list", "SpecificObjective.list", "EntityType.list", "LocationType.list"})
    void screens();
}