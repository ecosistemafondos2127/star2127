package es.redeco.star2127.temporalversioning;

import es.redeco.star2127.entity.EditionStatus;
import es.redeco.star2127.entity.StandardBitemporalEntity;
import io.jmix.core.DataManager;
import io.jmix.flowui.Dialogs;
import io.jmix.flowui.action.DialogAction;
import io.jmix.flowui.app.inputdialog.DialogActions;
import io.jmix.flowui.app.inputdialog.DialogOutcome;
import io.jmix.flowui.kit.action.ActionPerformedEvent;
import io.jmix.flowui.model.DataContext;
import io.jmix.flowui.view.StandardDetailView;
import io.jmix.flowui.view.Subscribe;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import static io.jmix.flowui.app.inputdialog.InputParameter.booleanParameter;
import static io.jmix.flowui.app.inputdialog.InputParameter.localDateTimeParameter;

public class StandardBitemporalDetailView<T extends StandardBitemporalEntity> extends StandardDetailView<T> {


    @Autowired
    private DataManager dataManager;

    @Autowired
    private Dialogs dialogs;

    public void getEffectiveDates(T sec) {
        if (sec.getEditVersion() == EditionStatus.OPEN_MAJOR) {
            dialogs.createInputDialog(this)
                    .withHeader("Obtener Fechas de Validez")
                    .withParameters(
                            localDateTimeParameter("beginInstant")
                                    .withLabel("Fecha inicio")
                                    .withRequired(true),
                            booleanParameter("hasEndInstant")
                                    .withLabel("¿Existe fecha final?")
                                    .withRequired(true),
                            localDateTimeParameter("endInstant")
                                    .withLabel("Fecha final")
                                    .withRequired(false)
                    )
                    .withActions(DialogActions.OK_CANCEL)
                    .withCloseListener(closeEvent -> {
                        if (closeEvent.closedWith(DialogOutcome.OK)) {
                            LocalDateTime from = closeEvent.getValue("beginInstant");
                            Boolean hasEnd = closeEvent.getValue("hasEndInstant");
                            LocalDateTime to;
                            if (Boolean.TRUE.equals(hasEnd))
                                to = closeEvent.getValue("endInstant");
                            else
                                to = BitemporalEntity.INFINITE;
                            closeMajor(sec, from, hasEnd, to);
                            closeWithSave();
                        } else
                            closeWithDiscard();
                    })
                    .open();
        } else if (sec.getEditVersion() == EditionStatus.OPEN_MINOR) {
            dialogs.createOptionDialog()
                    .withHeader("Por favor, confirme")
                    .withText("¿Desea cerrar la versión?")
                    .withActions(
                            new DialogAction(DialogAction.Type.YES)
                                    .withHandler(e ->
                                    {
                                        closeMinor(sec);
                                        closeWithSave();
                                    }),
                            new DialogAction(DialogAction.Type.NO)
                    )
                    .open();
        }
    }
    private  void closeMajor(T sec, LocalDateTime from, Boolean hasEnd, LocalDateTime to) {
 /*
        Optional<T> regopt = dataManager.load(T.class)
                .query("e.name = ?1 and e.status = 'Valid' order by e.effectivePeriod.from desc", sec.getName())
                .optional();

        // is there a previous version?
        if (regopt.isPresent()) {
            T reg2 = regopt.get();
            EffectivePeriod tmp = reg2.getEffectivePeriod();
            reg2.setEffectivePeriod(tmp.getFrom(), from);
            reg2.setStatus(EditionStatus.NOT_VALID);
            dataManager.save(reg2);
        }
        sec.setEffectivePeriod(from, to);
        sec.setRecordPeriod(LocalDateTime.now(), RecordPeriod.INFINITE);
        sec.setStatus(EditionStatus.VALID);
 */
    }

    private  void closeMinor(T sec) {
 /*
        Optional<T> regopt = dataManager.load(T.class)
                .query("e.name = ?1 and e.status = 'Valid' order by e.effectivePeriod.from desc", sec.getName())
                .optional();

        // is there a previous version?
        if (regopt.isPresent()) {
            T reg2 = regopt.get();
            reg2.setStatus(EditionStatus.NOT_VALID);
            RecordPeriod tmp = reg2.getRecordPeriod();
            reg2.setRecordPeriod(tmp.getFrom(), LocalDateTime.now());
            dataManager.save(reg2);

        }

        sec.setRecordPeriod(LocalDateTime.now(), RecordPeriod.INFINITE);
        sec.setStatus(EditionStatus.VALID);
  */
    }
}
