package es.redeco.star2127.temporalversioning;


/**
 * Simple SemVer version parsing and comparison in Java
 * <p/>
 * Example:
 * <p/>
 * Increment version '1.2.12' (with prefix '1.2.') becomes '1.2.13'
 */
public class Version implements Comparable<Version> {

    public final int[] numbers;

    public final String version;

    public Version(String version) {
        final String[] split = version.split("\\-")[0].split("\\.");
        numbers = new int[split.length];
        for (int i = 0; i < split.length; i++) {
            numbers[i] = Integer.parseInt(split[i]);
        }
        this.version = version;
    }

    @Override
    public int compareTo(Version another) {
        final int maxLength = Math.max(numbers.length, another.numbers.length);
        for (int i = 0; i < maxLength; i++) {
            final int left = i < numbers.length ? numbers[i] : 0;
            final int right = i < another.numbers.length ? another.numbers[i] : 0;
            if (left != right) {
                return left < right ? -1 : 1;
            }
        }
        return 0;
    }

    /** Retrieve the incremented major version number */
    public String getIncrementedMajorVersion() {
        int[] intArr = new int[2];
        intArr[0] = numbers[0] +1;
        intArr[1] = 0;
        return IntArrayToString(intArr);
    }

    /** Retrieve the incremented minor version number */
    public String getIncrementedMinorVersion() {
        int[] intArr = new int[2];
        intArr[0] = numbers[0];
        intArr[1] = numbers[1] +1;
        return IntArrayToString(intArr);
    }

    private String IntArrayToString(int[] array) {
        StringBuilder stringBuilder = new StringBuilder();
        for(int i =0; i < array.length; i++) {
            stringBuilder.append(array[i]);
            if (i != array.length-1)
                stringBuilder.append(".");
        }
        return stringBuilder.toString();
    }
}