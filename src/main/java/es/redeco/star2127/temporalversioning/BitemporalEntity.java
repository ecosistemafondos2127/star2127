package es.redeco.star2127.temporalversioning;

import es.redeco.star2127.entity.EditionStatus;
import es.redeco.star2127.entity.RecordState;

import java.time.LocalDateTime;

/**
 * Interface for Bitemporal entities.
 */
public interface BitemporalEntity {
    public static final LocalDateTime INFINITE = LocalDateTime.of(2999, 12, 31, 23, 59, 59);

    String getVersionNumber();
    void setVersionNumber(String versionNumber);


    EditionStatus getEditVersion();

    void setEditVersion(EditionStatus status);

    RecordState getRecordStatus();

    void setRecordStatus(RecordState recordStatus);

    LocalDateTime getVt_End();

    void setVt_End(LocalDateTime vt_End);

    LocalDateTime getVt_Start();

    void setVt_Start(LocalDateTime vt_Start);

    LocalDateTime getTt_Start();

    void setTt_Start(LocalDateTime tt_Start);

    void setTt_End(LocalDateTime tt_End);

    LocalDateTime getTt_End();
}