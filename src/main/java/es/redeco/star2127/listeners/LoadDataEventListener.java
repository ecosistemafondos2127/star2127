package es.redeco.star2127.listeners;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import es.redeco.star2127.conversion.*;
import es.redeco.star2127.entity.*;
import io.jmix.core.DataManager;
import io.jmix.core.EntitySerialization;
import io.jmix.core.Metadata;
import io.jmix.core.Resources;
import io.jmix.core.EntitySerializationOption;
import io.jmix.core.impl.importexport.EntityImportException;
import io.jmix.core.metamodel.model.MetaClass;
import io.jmix.core.querycondition.PropertyCondition;
import io.jmix.core.security.CurrentAuthentication;
import io.jmix.core.security.SystemAuthenticator;
import org.apache.commons.compress.archivers.zip.ZipArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Component
public class LoadDataEventListener implements ApplicationListener<ApplicationStartedEvent> {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(LoadDataEventListener.class);

    public static String TESTDATA_FILE_PATTERN = "data/test/*.zip";
    public static String SEEDDATA_FILE_PATTERN = "data/seed/*.zip";

    @Autowired
    Resources resources;

    @Autowired
    Metadata metadata;

    @Autowired
    private DataManager dataManager;

    @Autowired
    private SystemAuthenticator systemAuthenticator;

    @Autowired
    private CurrentAuthentication currentAuthentication;

    @Autowired
    private NutsConversion nutsConversor;

    @Autowired
    private CountryConversion countryConversor;

    @Autowired
    private ConstantConversion constantConversion;

    @Autowired
    private EntityTypeConversion entityTypeConversion;

    @Autowired
    private LawTypeConversion lawTypeConversion;
    @Autowired
    private LegalTypeConversion legalTypeConversion;
    @Autowired
    private NIFTypeConversion NIFTypeConversion;
    @Autowired
    private StateDIR3Conversion stateDIR3Conversion;
    @Autowired
    private CNClassificationConversion CNClassificationConversion;

    @Autowired
    private AdministrativeLevelConversion administrativeLevelConversion;

    @Autowired
    private EconomicActivityConversion economicActivityConversion;

    @Autowired
    private MacroRegionalSeaBasinStrategiesConversion macroRegionalSeaBasinStrategiesConversion;

    @Autowired
    private GenderEqualityConversion genderEqualityConversion;

    @Autowired
    private TerritorialFocusConversion territorialFocusConversion;
    @Autowired
    private FormOfSupportConversion formOfSupportConversion;
    @Autowired
    private CategoryOfRegionConversion categoryOfRegionConversion;
    @Autowired
    private CategoryOfRegionByAACCConversion categoryOfRegionByAACCConversion;
    @Autowired
    private AutonomousCommunityConversion autonomousCommunityConversion;
    @Override
    public void onApplicationEvent(ApplicationStartedEvent event) {
        importSeedData();

        //if (globalConfig.testMode) {
        //    importTestdata();
        //}

        //clearConfigurationCache();
    }
    public void importSeedData() {
        importData(SEEDDATA_FILE_PATTERN);
    }

    @ManagedOperation
    public void importData(String filePattern) {
        systemAuthenticator.withUser("admin", () -> {
            UserDetails user = currentAuthentication.getUser();
            log.info("User: " + user.getUsername()); // admin
            try {

                log.info("Loading resources for: " + filePattern);

                Resource[] allZipResources = ResourcePatternUtils.getResourcePatternResolver(resources).getResources(filePattern);

                Arrays.stream(allZipResources).sorted(Comparator.comparing(Resource::getFilename)).forEach((r)-> {
                    log.info(r.getFilename());
                    importDataForResource(r);
                });

            } catch (IOException e) {
                log.error("Error", e);
            }
            return null;
        });
    }

    protected void importDataForResource(Resource resource) {
        try {
            log.info("Importing resource file located at: " + resource.getFile().getAbsolutePath());
            MetaClass entityClass = determineEntityClass(resource);
            if (entityClass != null) {
                byte[] zipBytes = IOUtils.toByteArray(resource.getInputStream());
                importEntitiesFromZIP(zipBytes, entityClass);

                log.info("Import successful for: " + resource.getFilename());
            }
            else {
                log.error("Class could not be found - import skipped for: " + resource.getFilename());
            }

        } catch (IOException e) {
            log.error("Class could not be loaded - import skipped for: " + resource.getFilename());
        }
    }
    protected MetaClass determineEntityClass(Resource resource) {

        try {
            String filename = resource.getFilename();
            assert filename != null;
            String[] filenameParts = filename.split("-");
            String classNameWithExtension = filenameParts[1];
            String className = classNameWithExtension.replace(".zip","");
            return metadata.getClass(className);
        }
        catch (Exception e) {
            return null;
        }
    }

    public Collection<Object> importEntitiesFromZIP(byte[] zipBytes, MetaClass entityClass) {
        Collection<Object> result = new ArrayList<>();
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(zipBytes);
        ZipArchiveInputStream archiveReader = new ZipArchiveInputStream(byteArrayInputStream);
        try {
            try {
                while (archiveReader.getNextZipEntry() != null) {
                    String json = new String(IOUtils.toByteArray(archiveReader), StandardCharsets.UTF_8);
                    JsonElement myjson = JsonParser.parseString(json);
                    switch (entityClass.getName()) {
                        case "Nuts":
                            result.addAll(saveNuts(myjson));
                            break;
                        case "Country":
                            result.addAll(saveCountry(myjson));
                            break;
                        case "Constant":
                            result.addAll(saveConstant(myjson));
                            break;
                        case "EntityType":
                            result.addAll(saveEntityType(myjson));
                            break;
                        case "LawType":
                            result.addAll(saveLawType(myjson));
                            break;
                        case "LegalType":
                            result.addAll(saveLegalType(myjson));
                            break;
                        case "NIFType":
                            result.addAll(saveNIFType(myjson));
                            break;
                        case "StateDIR3":
                            result.addAll(saveStateDIR3(myjson));
                            break;
                        case "CNClassification":
                            result.addAll(saveCNClassification(myjson));
                            break;
                        case "AdministrativeLevel":
                            result.addAll(saveAdministrativeLevel(myjson));
                            break;
                        case "EconomicActivity":
                            result.addAll(saveEconomicActivity(myjson));
                            break;
                        case "MacroRegionalSeaBasinStrategies":
                            result.addAll(saveMacroRegionalSeaBasinStrategies(myjson));
                            break;
                        case "GenderEquality":
                            result.addAll(saveGenderEquality(myjson));
                            break;
                        case "TerritorialFocus":
                            result.addAll(saveTerritorialFocus(myjson));
                            break;
                        case "FormOfSupport":
                            result.addAll(saveFormOfSupport(myjson));
                            break;
                        case "CategoryOfRegion":
                            result.addAll(saveCategoryOfRegion(myjson));
                            break;
                        case "CategoryOfRegionByAACC":
                            result.addAll(saveCategoryOfRegionByAACC(myjson));
                            break;
                        //TODO Add more tables
                        case "AutonomousCommunity":
                            result.addAll(saveAutonomousCommunity(myjson));
                            break;
                        default:
                            log.error("Entity class not found or not implemented: " + entityClass.getName());
                    }
                }
            } catch (IOException e) {
                throw new RuntimeException("Exception occurred while importing data", e);
            }
        } finally {
            IOUtils.closeQuietly(archiveReader);
        }
        return result;
    }

    Collection<Nuts> saveNuts(JsonElement json) {
        assert  json.isJsonArray();
        Collection<Nuts> result = null;
        try {
            result = nutsConversor.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<Country> saveCountry(JsonElement json) {
        assert  json.isJsonArray();
        Collection<Country> result = null;
        try {
            result = countryConversor.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<Constant> saveConstant(JsonElement json) {
        assert  json.isJsonArray();
        Collection<Constant> result = null;
        try {
            result = constantConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
    Collection<EntityType> saveEntityType(JsonElement json) {
        assert  json.isJsonArray();
        Collection<EntityType> result = null;
        try {
            result = entityTypeConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
    Collection<LawType> saveLawType(JsonElement json) {
        assert  json.isJsonArray();
        Collection<LawType> result = null;
        try {
            result = lawTypeConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
    Collection<LegalType> saveLegalType(JsonElement json) {
        assert  json.isJsonArray();
        Collection<LegalType> result = null;
        try {
            result = legalTypeConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
    Collection<NIFType> saveNIFType(JsonElement json) {
        assert  json.isJsonArray();
        Collection<NIFType> result = null;
        try {
            result = NIFTypeConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
    Collection<StateDIR3> saveStateDIR3(JsonElement json) {
        assert  json.isJsonArray();
        Collection<StateDIR3> result = null;
        try {
            result = stateDIR3Conversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<CNClassification> saveCNClassification(JsonElement json) {
        assert  json.isJsonArray();
        Collection<CNClassification> result = null;
        try {
            result = CNClassificationConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<AdministrativeLevel> saveAdministrativeLevel(JsonElement json) {
        assert  json.isJsonArray();
        Collection<AdministrativeLevel> result = null;
        try {
            result = administrativeLevelConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
    Collection<EconomicActivity> saveEconomicActivity(JsonElement json) {
        assert  json.isJsonArray();
        Collection<EconomicActivity> result = null;
        try {
            result = economicActivityConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
    Collection<MacroRegionalSeaBasinStrategies> saveMacroRegionalSeaBasinStrategies(JsonElement json) {
        assert  json.isJsonArray();
        Collection<MacroRegionalSeaBasinStrategies> result = null;
        try {
            result = macroRegionalSeaBasinStrategiesConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<GenderEquality> saveGenderEquality(JsonElement json) {
        assert  json.isJsonArray();
        Collection<GenderEquality> result = null;
        try {
            result = genderEqualityConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<TerritorialFocus> saveTerritorialFocus(JsonElement json) {
        assert  json.isJsonArray();
        Collection<TerritorialFocus> result = null;
        try {
            result = territorialFocusConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<FormOfSupport> saveFormOfSupport(JsonElement json) {
        assert  json.isJsonArray();
        Collection<FormOfSupport> result = null;
        try {
            result = formOfSupportConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<CategoryOfRegion> saveCategoryOfRegion(JsonElement json) {
        assert  json.isJsonArray();
        Collection<CategoryOfRegion> result = null;
        try {
            result = categoryOfRegionConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
    Collection<CategoryOfRegionByAACC> saveCategoryOfRegionByAACC(JsonElement json) {
        assert  json.isJsonArray();
        Collection<CategoryOfRegionByAACC> result = null;
        try {
            result = categoryOfRegionByAACCConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }

    Collection<AutonomousCommunity> saveAutonomousCommunity(JsonElement json) {
        assert  json.isJsonArray();
        Collection<AutonomousCommunity> result = null;
        try {
            result = autonomousCommunityConversion.ConvertArrayFrom(json.getAsJsonArray());
            result.forEach(entity -> dataManager.save(entity));
        } catch (Exception e) {
            log.error("Error saving entities", e);
        }
        return result;
    }
}