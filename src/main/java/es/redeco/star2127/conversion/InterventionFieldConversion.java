package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.InterventionField;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.InterventionFieldFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class InterventionFieldConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(InterventionFieldConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<InterventionField> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(InterventionField.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public InterventionField ConvertFrom(JsonObject jsonObj)
    {
        InterventionFieldFondosDTO obj = new Gson().fromJson(jsonObj,  InterventionFieldFondosDTO.class);

        // check if entity already exists
        Collection<InterventionField> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        InterventionField entity = dataManager.create(InterventionField.class);

        //TODO

        return entity;
    }

    public Collection<InterventionField> ConvertArrayFrom(JsonArray json) {
        Collection<InterventionField> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            InterventionField entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
