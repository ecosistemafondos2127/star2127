package es.redeco.star2127.conversion;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.AutonomousCommunity;
import es.redeco.star2127.entity.Country;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.AutonomousCommunityFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Component
public class AutonomousCommunityConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(AutonomousCommunityConversion.class);

    @Autowired
    private DataManager dataManager;

    public List<AutonomousCommunity> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(AutonomousCommunity.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }
    private Nuts findNutsbyCodeNuts(String codeNUTS)
    {
        List<Nuts> result = dataManager.load(Nuts.class)
                .condition(PropertyCondition.contains("codeNuts", codeNUTS))
                .list();
        if (!result.isEmpty())
            return result.get(0);
        else
            return null;
    }

    private Country findCountrysbyNUTS1(String codeNUTS1)
    {
        Nuts nuts=findNutsbyCodeNuts(codeNUTS1);
        if (nuts!=null) {
            List<Country> result = dataManager.load(Country.class)
                    .condition(PropertyCondition.equal("codeNUTS1", nuts))
                    .list();
            if (!result.isEmpty())
                return result.get(0);
            else
                return null;
        }else{
            return null;
        }
    }
    public AutonomousCommunity ConvertFrom(JsonObject jsonObj)
    {
        AutonomousCommunityFondosDTO obj = new Gson().fromJson(jsonObj,  AutonomousCommunityFondosDTO.class);

        // check if entity already exists
        Collection<AutonomousCommunity> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        AutonomousCommunity entity = dataManager.create(AutonomousCommunity.class);

        // mandatory fields

        entity.setFunds2127id(obj.getId());
        entity.setCodeNuts2(findNutsbyCodeNuts(obj.getCodigoNut2()));//Foreign key
        entity.setNameES(obj.getNombre());
        entity.setAcronymES(obj.getNombreLocal());
        entity.setIsoCode31662(obj.getCodigoIso());
        entity.setCountry(findCountrysbyNUTS1(obj.getCodigoNutI()));//Foreign key

        return entity;
    }

    public Collection<AutonomousCommunity> ConvertArrayFrom(JsonArray json) {
        Collection<AutonomousCommunity> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            AutonomousCommunity entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
