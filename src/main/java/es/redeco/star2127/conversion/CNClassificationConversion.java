package es.redeco.star2127.conversion;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.CNClassification;
import es.redeco.star2127.entity.StateDIR3;
import es.redeco.star2127.fondos2127entity.CNClassificationFondosDTO;
import es.redeco.star2127.fondos2127entity.StateDIR3FondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class CNClassificationConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CNClassificationConversion.class);

    @Autowired
    private DataManager dataManager;

    public List<CNClassification> loadById(Integer code) {
        try {
            return dataManager.load(CNClassification.class)
                    .condition(PropertyCondition.contains("code", code.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with code =" + code.toString());
            throw  e;
        }
    }


    public CNClassification ConvertFrom(JsonObject jsonObj)
    {
        CNClassificationFondosDTO obj = new Gson().fromJson(jsonObj,  CNClassificationFondosDTO.class);

        // check if entity already exists
        Collection<CNClassification> entityById = loadById(obj.getCodigo());
        if (!entityById.isEmpty()) return null;
        CNClassification entity = dataManager.create(CNClassification.class);

        entity.setCode(obj.getCodigo());
        entity.setNameES(obj.getNombre());

        return entity;
    }

    public Collection<CNClassification> ConvertArrayFrom(JsonArray json) {
        Collection<CNClassification> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            CNClassification entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
