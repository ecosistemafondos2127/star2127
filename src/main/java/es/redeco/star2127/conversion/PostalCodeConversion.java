package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.PostalCode;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.PostalCodeFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class PostalCodeConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(PostalCodeConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<PostalCode> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(PostalCode.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public PostalCode ConvertFrom(JsonObject jsonObj)
    {
        PostalCodeFondosDTO obj = new Gson().fromJson(jsonObj,  PostalCodeFondosDTO.class);

        // check if entity already exists
        Collection<PostalCode> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        PostalCode entity = dataManager.create(PostalCode.class);

        //TODO

        return entity;
    }

    public Collection<PostalCode> ConvertArrayFrom(JsonArray json) {
        Collection<PostalCode> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            PostalCode entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
