package es.redeco.star2127.conversion;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.NIFType;
import es.redeco.star2127.fondos2127entity.NIFTypeFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class NIFTypeConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(NIFTypeConversion.class);

    @Autowired
    private DataManager dataManager;

    public List<NIFType> loadById(String code) {
        try {
            return dataManager.load(NIFType.class)
                    .condition(PropertyCondition.contains("code", code))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with code =" + code);
            throw  e;
        }
    }


    public NIFType ConvertFrom(JsonObject jsonObj)
    {
        NIFTypeFondosDTO obj = new Gson().fromJson(jsonObj,  NIFTypeFondosDTO.class);

        // check if entity already exists
        Collection<NIFType> entityById = loadById(obj.getCodigo());
        if (!entityById.isEmpty()) return null;
        NIFType entity = dataManager.create(NIFType.class);

        entity.setCode(obj.getCodigo());
        entity.setNameES(obj.getNombre());

        return entity;
    }

    public Collection<NIFType> ConvertArrayFrom(JsonArray json) {
        Collection<NIFType> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            NIFType entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
