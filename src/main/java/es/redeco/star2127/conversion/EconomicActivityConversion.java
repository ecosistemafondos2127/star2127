package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.EconomicActivity;
import es.redeco.star2127.fondos2127entity.EconomicActivityFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class EconomicActivityConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(EconomicActivityConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<EconomicActivity> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(EconomicActivity.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public EconomicActivity ConvertFrom(JsonObject jsonObj)
    {
        EconomicActivityFondosDTO obj = new Gson().fromJson(jsonObj,  EconomicActivityFondosDTO.class);

        // check if entity already exists
        Collection<EconomicActivity> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        EconomicActivity entity = dataManager.create(EconomicActivity.class);

        entity.setFunds2127id(obj.getId());
        entity.setCode(obj.getCodigo());
        entity.setCodeSfc(obj.getCodigoSfc());
        entity.setNameES(obj.getNombre());
        entity.setNameEN(obj.getNombreEn());
        entity.setDescriptionES(obj.getDescripcion());

        return entity;
    }

    public Collection<EconomicActivity> ConvertArrayFrom(JsonArray json) {
        Collection<EconomicActivity> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            EconomicActivity entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
