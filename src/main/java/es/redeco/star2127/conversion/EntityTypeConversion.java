package es.redeco.star2127.conversion;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.EntityType;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class EntityTypeConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(EntityTypeConversion.class);

    @Autowired
    private DataManager dataManager;

    public List<EntityType> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(EntityType.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }
    public EntityType ConvertFrom(JsonObject jsonObj)
    {
        EntityType entity = dataManager.create(EntityType.class);

        // mandatory fields
        Integer funds2127id = jsonObj.get("id").getAsInt();
        String code = jsonObj.get("code").getAsString();
        String tableName=jsonObj.get("tableName").getAsString();

        // check if entity already exists
        Collection<EntityType> entityById = loadById(funds2127id);
        if (!entityById.isEmpty()) return null;

        entity.setFunds2127id(funds2127id);
        entity.setCode(code);
        entity.setTableName(tableName);

        return entity;
    }
    public Collection<EntityType> ConvertArrayFrom(JsonArray json) {
        Collection<EntityType> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            EntityType entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}

