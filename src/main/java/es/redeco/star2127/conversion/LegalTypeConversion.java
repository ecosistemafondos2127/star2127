package es.redeco.star2127.conversion;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.LegalType;
import es.redeco.star2127.fondos2127entity.LegalTypeFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class LegalTypeConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(LegalTypeConversion.class);

    @Autowired
    private DataManager dataManager;

    public List<LegalType> loadById(Integer code) {
        try {
            return dataManager.load(LegalType.class)
                    .condition(PropertyCondition.contains("code", code.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with code =" + code.toString());
            throw  e;
        }
    }


    public LegalType ConvertFrom(JsonObject jsonObj)
    {
        LegalTypeFondosDTO obj = new Gson().fromJson(jsonObj,  LegalTypeFondosDTO.class);

        // check if entity already exists
        Collection<LegalType> entityById = loadById(obj.getCodigo());
        if (!entityById.isEmpty()) return null;
        LegalType entity = dataManager.create(LegalType.class);

        entity.setCode(obj.getCodigo());
        entity.setNameES(obj.getNombre());

        return entity;
    }

    public Collection<LegalType> ConvertArrayFrom(JsonArray json) {
        Collection<LegalType> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            LegalType entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}

