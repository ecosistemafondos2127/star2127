package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.FundGroup;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.FundGroupFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class FundGroupConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(FundGroupConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<FundGroup> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(FundGroup.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public FundGroup ConvertFrom(JsonObject jsonObj)
    {
        FundGroupFondosDTO obj = new Gson().fromJson(jsonObj,  FundGroupFondosDTO.class);

        // check if entity already exists
        /* TODO
        Collection<FundGroup> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
         */
        FundGroup entity = dataManager.create(FundGroup.class);

        //TODO

        return entity;
    }

    public Collection<FundGroup> ConvertArrayFrom(JsonArray json) {
        Collection<FundGroup> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            FundGroup entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
