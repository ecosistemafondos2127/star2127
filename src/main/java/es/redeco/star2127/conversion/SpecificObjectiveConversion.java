package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.SpecificObjective;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.SpecificObjectiveFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class SpecificObjectiveConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(SpecificObjectiveConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<SpecificObjective> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(SpecificObjective.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public SpecificObjective ConvertFrom(JsonObject jsonObj)
    {
        SpecificObjectiveFondosDTO obj = new Gson().fromJson(jsonObj,  SpecificObjectiveFondosDTO.class);

        // check if entity already exists
        Collection<SpecificObjective> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        SpecificObjective entity = dataManager.create(SpecificObjective.class);

        //TODO

        return entity;
    }

    public Collection<SpecificObjective> ConvertArrayFrom(JsonArray json) {
        Collection<SpecificObjective> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            SpecificObjective entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
