package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.Fund;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.FundFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class FundConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(FundConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<Fund> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(Fund.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public Fund ConvertFrom(JsonObject jsonObj)
    {
        FundFondosDTO obj = new Gson().fromJson(jsonObj,  FundFondosDTO.class);

        // check if entity already exists
        Collection<Fund> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        Fund entity = dataManager.create(Fund.class);

        //TODO

        return entity;
    }

    public Collection<Fund> ConvertArrayFrom(JsonArray json) {
        Collection<Fund> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            Fund entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
