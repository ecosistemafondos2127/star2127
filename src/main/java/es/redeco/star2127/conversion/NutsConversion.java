package es.redeco.star2127.conversion;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.entity.NutsEnum;
import es.redeco.star2127.fondos2127entity.NutsFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class NutsConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(NutsConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<Nuts> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(Nuts.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }

    public Nuts ConvertFrom(JsonObject jsonObj)
    {
        NutsFondosDTO obj = new Gson().fromJson(jsonObj,  NutsFondosDTO.class);

        // check if entity already exists
        Collection<Nuts> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        Nuts entity = dataManager.create(Nuts.class);

        // mandatory fields
        entity.setFunds2127id(obj.getId());
        entity.setCodeNuts(obj.getCodigo());
        entity.setNameES(obj.getNombre());
        entity.setNameEN(obj.getNombrePais()); // TODO mirar esto. está puesto como obligatorio y no tenemos esa info!!!
        if (obj.getCodigo().length()==2) {
            entity.setNutsLevel(NutsEnum.NUTS0);
        } else if (obj.getCodigo().length()==3) {
            entity.setNutsLevel(NutsEnum.NUTS1);
        } else if (obj.getCodigo().length()==4) {
            entity.setNutsLevel(NutsEnum.NUTS2);
        } else  {
            entity.setNutsLevel(NutsEnum.NUTS3);
        }

        // Opcional fields
        if (obj.getNombrePais() != null) {
           // TODO entity.setNameES(obj.getNombrePais());
        }

        return entity;
    }

    public Collection<Nuts> ConvertArrayFrom(JsonArray json) {
        Collection<Nuts> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            Nuts entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
