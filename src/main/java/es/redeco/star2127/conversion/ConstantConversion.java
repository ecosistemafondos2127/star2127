package es.redeco.star2127.conversion;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.Constant;
import io.jmix.core.DataManager;

import io.jmix.core.querycondition.LogicalCondition;
import io.jmix.core.querycondition.PropertyCondition;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class ConstantConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(ConstantConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<Constant> loadById(String rvDomain, String rvLowValue) {
        try {
            List<Constant> list= null;
            return dataManager.load(Constant.class)
                    .condition(LogicalCondition.and(PropertyCondition.contains("rvDomain", rvDomain),
                                                    PropertyCondition.contains("rvLowValue", rvLowValue)))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + rvDomain+ " rvLowValue ="+rvLowValue);
            throw  e;
        }
    }
    public Constant ConvertFrom(JsonObject jsonObj)
    {
        Constant entity = dataManager.create(Constant.class);

        // mandatory fields
        String rvDomain = jsonObj.get("rvDomain").getAsString();
        String rvLowValue = jsonObj.get("rvLowValue").getAsString();
        String rvHighValue=null;
        if (jsonObj.has("rvHighValue") && !(jsonObj.get("rvHighValue") instanceof JsonNull)) {
            rvHighValue = jsonObj.get("rvHighValue").getAsString();

        }
        String rvAbbreviation = jsonObj.get("rvAbbreviation").getAsString();
        String rvMeaning = jsonObj.get("rvMeaning").getAsString();

        // check if entity already exists
        Collection<Constant> entityById = loadById(rvDomain,rvLowValue);
        if (!entityById.isEmpty()) return null;

        entity.setRvDomain(rvDomain);
        entity.setRvLowValue(rvLowValue);
        entity.setRvHighValue(rvHighValue);
        entity.setRvAbbreviation(rvAbbreviation);
        entity.setRvMeaning(rvMeaning);

        return entity;
    }
    public Collection<Constant> ConvertArrayFrom(JsonArray json) {
        Collection<Constant> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            Constant entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
