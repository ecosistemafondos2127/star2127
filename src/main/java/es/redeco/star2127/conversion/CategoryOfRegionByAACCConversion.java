package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.CategoryOfRegionByAACC;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.CategoryOfRegionByAACCFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class CategoryOfRegionByAACCConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CategoryOfRegionByAACCConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<CategoryOfRegionByAACC> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(CategoryOfRegionByAACC.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public CategoryOfRegionByAACC ConvertFrom(JsonObject jsonObj)
    {
        CategoryOfRegionByAACCFondosDTO obj = new Gson().fromJson(jsonObj,  CategoryOfRegionByAACCFondosDTO.class);

        // check if entity already exists
        Collection<CategoryOfRegionByAACC> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        CategoryOfRegionByAACC entity = dataManager.create(CategoryOfRegionByAACC.class);

        entity.setFunds2127id(obj.getId());
        entity.setCodeNut2(obj.getCodigoNut2());
        entity.setCodeNutII(obj.getCodigoNutII());
        entity.setCodeRegionCat(obj.getCodigoCatRegion());
        entity.setDescriptionES(obj.getDescripcion());
        entity.setNameRegionCat(obj.getNomCatRegion());
        entity.setNameNutII(obj.getNomNUTII());
        entity.setPercentage(obj.getPorcentaje());

        return entity;
    }

    public Collection<CategoryOfRegionByAACC> ConvertArrayFrom(JsonArray json) {
        Collection<CategoryOfRegionByAACC> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            CategoryOfRegionByAACC entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
