package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.EnablingConditions;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.EnablingConditionsFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class EnablingConditionsConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(EnablingConditionsConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<EnablingConditions> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(EnablingConditions.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public EnablingConditions ConvertFrom(JsonObject jsonObj)
    {
        EnablingConditionsFondosDTO obj = new Gson().fromJson(jsonObj,  EnablingConditionsFondosDTO.class);

        // check if entity already exists
        /* TODO
        Collection<EnablingConditions> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
         */
        EnablingConditions entity = dataManager.create(EnablingConditions.class);

        //TODO

        return entity;
    }

    public Collection<EnablingConditions> ConvertArrayFrom(JsonArray json) {
        Collection<EnablingConditions> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            EnablingConditions entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
