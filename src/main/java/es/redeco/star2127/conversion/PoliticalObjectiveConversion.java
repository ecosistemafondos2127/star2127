package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.PoliticalObjective;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.PoliticalObjectiveFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class PoliticalObjectiveConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(PoliticalObjectiveConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<PoliticalObjective> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(PoliticalObjective.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public PoliticalObjective ConvertFrom(JsonObject jsonObj)
    {
        PoliticalObjectiveFondosDTO obj = new Gson().fromJson(jsonObj,  PoliticalObjectiveFondosDTO.class);

        // check if entity already exists
        Collection<PoliticalObjective> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        PoliticalObjective entity = dataManager.create(PoliticalObjective.class);

        //TODO

        return entity;
    }

    public Collection<PoliticalObjective> ConvertArrayFrom(JsonArray json) {
        Collection<PoliticalObjective> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            PoliticalObjective entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
