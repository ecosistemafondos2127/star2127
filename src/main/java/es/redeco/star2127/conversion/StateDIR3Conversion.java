package es.redeco.star2127.conversion;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.StateDIR3;
import es.redeco.star2127.fondos2127entity.StateDIR3FondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class StateDIR3Conversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(StateDIR3Conversion.class);

    @Autowired
    private DataManager dataManager;

    public List<StateDIR3> loadById(String code) {
        try {
            return dataManager.load(StateDIR3.class)
                    .condition(PropertyCondition.contains("code", code))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with code =" + code);
            throw  e;
        }
    }


    public StateDIR3 ConvertFrom(JsonObject jsonObj)
    {
        StateDIR3FondosDTO obj = new Gson().fromJson(jsonObj,  StateDIR3FondosDTO.class);

        // check if entity already exists
        Collection<StateDIR3> entityById = loadById(obj.getCodigo());
        if (!entityById.isEmpty()) return null;
        StateDIR3 entity = dataManager.create(StateDIR3.class);

        entity.setCode(obj.getCodigo());
        entity.setNameES(obj.getNombre());

        return entity;
    }

    public Collection<StateDIR3> ConvertArrayFrom(JsonArray json) {
        Collection<StateDIR3> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            StateDIR3 entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
