package es.redeco.star2127.conversion;

import com.google.gson.*;
import es.redeco.star2127.entity.Country;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.CountryFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class CountryConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CountryConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<Country> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(Country.class)
                        .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                        .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }

    private Nuts findNutsbyCodeNuts(String    codeNUTS1)
    {
        List<Nuts> result = dataManager.load(Nuts.class)
                                    .condition(PropertyCondition.equal("codeNuts", codeNUTS1))
                                    .list();
        if (!result.isEmpty())
            return result.get(0);
        else
            return null;
    }

    public Country ConvertFrom(JsonObject jsonObj)
    {
        CountryFondosDTO obj = new Gson().fromJson(jsonObj,  CountryFondosDTO.class);

        // check if entity already exists
        Collection<Country> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        Country entity = dataManager.create(Country.class);

        // mandatory fields
        Nuts nutsFK = findNutsbyCodeNuts(obj.getCodigoIsoChar2()); // Foreign key
        if (nutsFK == null) return null;
        entity.setFunds2127id(obj.getId());
        entity.setNameES(obj.getNombre());
        entity.setCodeNUTS1(nutsFK);
        if (obj.getEsUe() == null)
            entity.setIsEU(true); //TODO set true by default, no information is provided by Fondos2127
        else
            entity.setIsEU(obj.getEsUe());
        entity.setIsoCodeAlpha2(obj.getCodigoIsoChar2());
        entity.setIsoCodeAlpha3(obj.getCodigoIsoChar3());

        // Opcional fields
        if (obj.getCodigoIsoNum() != null) {
            //TODO entity.setIsoNum(obj.getCodigoIsoNum());
        }

        return entity;
    }

    public Collection<Country> ConvertArrayFrom(JsonArray json) {
        Collection<Country> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            Country entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
