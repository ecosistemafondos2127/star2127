package es.redeco.star2127.conversion;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import es.redeco.star2127.entity.AdministrativeLevel;
import es.redeco.star2127.fondos2127entity.AdministrativeLevelFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class AdministrativeLevelConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(AdministrativeLevelConversion.class);

    @Autowired
    private DataManager dataManager;

    public List<AdministrativeLevel> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(AdministrativeLevel.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }

    public AdministrativeLevel ConvertFrom(JsonObject jsonObj)
    {
        AdministrativeLevelFondosDTO obj = new Gson().fromJson(jsonObj,  AdministrativeLevelFondosDTO.class);

        // check if entity already exists
        Collection<AdministrativeLevel> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        AdministrativeLevel entity = dataManager.create(AdministrativeLevel.class);

        // mandatory fields

        entity.setFunds2127id(obj.getId());
        entity.setCode(obj.getCodigo());
        entity.setNameES(obj.getNombre());

        return entity;
    }

    public Collection<AdministrativeLevel> ConvertArrayFrom(JsonArray json) {
        Collection<AdministrativeLevel> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            AdministrativeLevel entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
