package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.FormOfSupport;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.FormOfSupportFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component


public class FormOfSupportConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(FormOfSupportConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<FormOfSupport> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(FormOfSupport.class)
                    .condition(PropertyCondition.contains("funds2127ID", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public FormOfSupport ConvertFrom(JsonObject jsonObj)
    {
        FormOfSupportFondosDTO obj = new Gson().fromJson(jsonObj,  FormOfSupportFondosDTO.class);

        // check if entity already exists
        Collection<FormOfSupport> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        FormOfSupport entity = dataManager.create(FormOfSupport.class);

        entity.setFunds2127ID(obj.getId());
        entity.setCode(obj.getCodigo());
        entity.setNameES(obj.getNombre());

        return entity;
    }

    public Collection<FormOfSupport> ConvertArrayFrom(JsonArray json) {
        Collection<FormOfSupport> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            FormOfSupport entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
