package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.MacroRegionalSeaBasinStrategies;
import es.redeco.star2127.fondos2127entity.MacroRegionalSeaBasinStrategiesFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component
public class MacroRegionalSeaBasinStrategiesConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(MacroRegionalSeaBasinStrategiesConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<MacroRegionalSeaBasinStrategies> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(MacroRegionalSeaBasinStrategies.class)
                    .condition(PropertyCondition.contains("funds2127ID", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public MacroRegionalSeaBasinStrategies ConvertFrom(JsonObject jsonObj)
    {
        MacroRegionalSeaBasinStrategiesFondosDTO obj = new Gson().fromJson(jsonObj,  MacroRegionalSeaBasinStrategiesFondosDTO.class);

        // check if entity already exists
        Collection<MacroRegionalSeaBasinStrategies> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        MacroRegionalSeaBasinStrategies entity = dataManager.create(MacroRegionalSeaBasinStrategies.class);

        entity.setFunds2127ID(obj.getId());
        entity.setCode(obj.getCodigo());
        entity.setCodeSfc(obj.getCodigoSfc());
        entity.setNameES(obj.getNombre());
        entity.setNameEN(obj.getNombreEn());
        entity.setDescriptionES(obj.getDescripcion());

        return entity;
    }

    public Collection<MacroRegionalSeaBasinStrategies> ConvertArrayFrom(JsonArray json) {
        Collection<MacroRegionalSeaBasinStrategies> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            MacroRegionalSeaBasinStrategies entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
