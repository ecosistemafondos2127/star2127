package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.CategoryOfRegion;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.CategoryOfRegionFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class CategoryOfRegionConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(CategoryOfRegionConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<CategoryOfRegion> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(CategoryOfRegion.class)
                    .condition(PropertyCondition.contains("funds2127id", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public CategoryOfRegion ConvertFrom(JsonObject jsonObj)
    {
        CategoryOfRegionFondosDTO obj = new Gson().fromJson(jsonObj,  CategoryOfRegionFondosDTO.class);

        // check if entity already exists
        Collection<CategoryOfRegion> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        CategoryOfRegion entity = dataManager.create(CategoryOfRegion.class);

        entity.setFunds2127id(obj.getId());
        entity.setCodeES(obj.getCodigo());
        entity.setCodeEN(obj.getCodigoEn());
        entity.setShortNameES(obj.getNombreCorto());
        entity.setShortNameEN(obj.getNombreCortoEn());
        entity.setNameES(obj.getNombre());
        entity.setNameEN(obj.getNombreEn());
        entity.setDescriptionES(obj.getDescripcion());
        entity.setDescriptionEN(obj.getDescripcionEn());
        entity.setOrder(obj.getOrden());

        return entity;
    }

    public Collection<CategoryOfRegion> ConvertArrayFrom(JsonArray json) {
        Collection<CategoryOfRegion> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            CategoryOfRegion entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
