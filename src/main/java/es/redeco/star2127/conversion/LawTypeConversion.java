package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.LawType;
import es.redeco.star2127.fondos2127entity.LawTypeFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class LawTypeConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(LawTypeConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<LawType> loadById(Integer code) {
        try {
            return dataManager.load(LawType.class)
                    .condition(PropertyCondition.contains("code", code.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with code =" + code.toString());
            throw  e;
        }
    }


    public LawType ConvertFrom(JsonObject jsonObj)
    {
        LawTypeFondosDTO obj = new Gson().fromJson(jsonObj,  LawTypeFondosDTO.class);

        // check if entity already exists
        Collection<LawType> entityById = loadById(obj.getCodigo());
        if (!entityById.isEmpty()) return null;
        LawType entity = dataManager.create(LawType.class);

        entity.setCode(obj.getCodigo());
        entity.setNameES(obj.getNombre());

        return entity;
    }

    public Collection<LawType> ConvertArrayFrom(JsonArray json) {
        Collection<LawType> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            LawType entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
