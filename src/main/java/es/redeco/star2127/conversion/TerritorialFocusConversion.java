package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.TerritorialFocus;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.TerritorialFocusFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class TerritorialFocusConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(TerritorialFocusConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<TerritorialFocus> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(TerritorialFocus.class)
                    .condition(PropertyCondition.contains("funds2127ID", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public TerritorialFocus ConvertFrom(JsonObject jsonObj)
    {
        TerritorialFocusFondosDTO obj = new Gson().fromJson(jsonObj,  TerritorialFocusFondosDTO.class);

        // check if entity already exists
        Collection<TerritorialFocus> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        TerritorialFocus entity = dataManager.create(TerritorialFocus.class);

        entity.setFunds2127ID(obj.getId());
        entity.setCodeApproach(obj.getCodigoEnfoque());
        entity.setCodeMechanism(obj.getCodigoMecanismo());
        entity.setNameES(obj.getNombre());

        return entity;
    }

    public Collection<TerritorialFocus> ConvertArrayFrom(JsonArray json) {
        Collection<TerritorialFocus> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            TerritorialFocus entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
