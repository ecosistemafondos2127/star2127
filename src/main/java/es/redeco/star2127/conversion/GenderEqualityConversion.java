package es.redeco.star2127.conversion;
import com.google.gson.*;
import es.redeco.star2127.entity.GenderEquality;
import es.redeco.star2127.entity.Nuts;
import es.redeco.star2127.fondos2127entity.GenderEqualityFondosDTO;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
@Component

public class GenderEqualityConversion {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(GenderEqualityConversion.class);

    @Autowired
    private DataManager dataManager;

    public  List<GenderEquality> loadById(Integer fondos2127Id) {
        try {
            return dataManager.load(GenderEquality.class)
                    .condition(PropertyCondition.contains("funds2127ID", fondos2127Id.toString()))
                    .list();
        } catch (Exception e)
        {
            log.error("Error found when looking for entity with id =" + fondos2127Id.toString());
            throw  e;
        }
    }


    public GenderEquality ConvertFrom(JsonObject jsonObj)
    {
        GenderEqualityFondosDTO obj = new Gson().fromJson(jsonObj,  GenderEqualityFondosDTO.class);

        // check if entity already exists
        Collection<GenderEquality> entityById = loadById(obj.getId());
        if (!entityById.isEmpty()) return null;
        GenderEquality entity = dataManager.create(GenderEquality.class);

        entity.setFunds2127ID(obj.getId());
        entity.setCode(obj.getCodigo());
        entity.setNameES(obj.getNombre());
        entity.setCoefficient(obj.getCoeficiente().floatValue());

        return entity;
    }

    public Collection<GenderEquality> ConvertArrayFrom(JsonArray json) {
        Collection<GenderEquality> result = new ArrayList<>();
        for (JsonElement pa : json.getAsJsonArray()) {
            JsonObject jsonObj = pa.getAsJsonObject();
            GenderEquality entity = ConvertFrom(jsonObj);

            if (entity != null)
                result.add(entity);
        }

        return result;
    }
}
