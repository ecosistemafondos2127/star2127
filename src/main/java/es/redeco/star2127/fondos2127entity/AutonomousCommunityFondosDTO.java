package es.redeco.star2127.fondos2127entity;

import liquibase.sqlgenerator.core.InsertGenerator;

public class AutonomousCommunityFondosDTO {

    public Integer id;
    private String codigo;
    private String codigoIso;
    private String codigoNutI;
    private String nombreNutI;
    private String codigoNut2;
    private String codigoSfc;
    private String nombre;
    private String nombreLocal;
    private String mfpNutsIII;
    private String mfpCatRegionesCcaa;

    public Integer getId(){
        return id;
    }
    public void setId(Integer id){
        this.id = id;
    }
    public String getCodigo(){
        return codigo;
    }
    public void setCodigo(String codigo){
        this.codigo = codigo;
    }
    public String getCodigoIso(){
        return codigoIso;
    }
    public void setCodigoIso(String codigoIso){
        this.codigoIso = codigoIso;
    }
    public String getCodigoNutI(){
        return codigoNutI;
    }
    public void setCodigoNutI(String codigoNutI){
        this.codigoNutI = codigoNutI;
    }
    public String getCodigoNut2(){
        return codigoNut2;
    }
    public void setCodigoNut2(String codigoNut2){
        this.codigoNut2 = codigoNut2;
    }
    public String getNombre(){
        return nombre;
    }
    public void setNombre(String nombreNutI){
        this.nombre = nombre;
    }
    public String getNombreNutI(){
        return nombreNutI;
    }
    public void setNombreNutI(String nombreNutI){
        this.nombreNutI = nombreNutI;
    }
    public String getNombreLocal(){
        return nombreLocal;
    }
    public void setNombreLocal(String nombreLocal){
        this.nombreLocal = nombreLocal;
    }
    public String getCodigoSfc(){
        return codigoSfc;
    }
    public void setCodigoSfc(String codigoSfc){
        this.codigoSfc = codigoSfc;
    }
    public String getMfpNutsIII(){
        return mfpNutsIII;
    }
    public void setMfpNutsIII(String mfpNutsIII){
        this.mfpNutsIII = mfpNutsIII;
    }
    public String getMfpCatRegionesCcaa(){
        return mfpCatRegionesCcaa;
    }
    public void setMfpCatRegionesCcaa(String mfpCatRegionesCcaa){
        this.mfpCatRegionesCcaa = mfpCatRegionesCcaa;
    }
}
