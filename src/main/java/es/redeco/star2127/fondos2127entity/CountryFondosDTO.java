package es.redeco.star2127.fondos2127entity;

public class CountryFondosDTO {
    public Integer id;
    public String codigoIsoChar2;
    public String codigoIsoChar3;
    public Integer codigoIsoNum;
    public String codigoSfc;
    public String descripcion;
    public Boolean esUe;
    public String nombre;
    public String nombreCorto;
    public String nombreEn;
    public String nombreCortoEn;
    public String nombreLocal;
    public String nombreCortoLocal;
    public String nutsI;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigoIsoChar2() {
        return codigoIsoChar2;
    }

    public void setCodigoIsoChar2(String codigoIsoChar2) {
        this.codigoIsoChar2 = codigoIsoChar2;
    }

    public String getCodigoIsoChar3() {
        return codigoIsoChar3;
    }

    public void setCodigoIsoChar3(String codigoIsoChar3) {
        this.codigoIsoChar3 = codigoIsoChar3;
    }

    public Integer getCodigoIsoNum() {
        return codigoIsoNum;
    }

    public void setCodigoIsoNum(Integer codigoIsoNum) {
        this.codigoIsoNum = codigoIsoNum;
    }

    public String getCodigoSfc() {
        return codigoSfc;
    }

    public void setCodigoSfc(String codigoSfc) {
        this.codigoSfc = codigoSfc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEsUe() {
        return esUe;
    }

    public void setEsUe(Boolean esUe) {
        this.esUe = esUe;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getNombreEn() {
        return nombreEn;
    }

    public void setNombreEn(String nombreEn) {
        this.nombreEn = nombreEn;
    }

    public String getNombreCortoEn() {
        return nombreCortoEn;
    }

    public void setNombreCortoEn(String nombreCortoEn) {
        this.nombreCortoEn = nombreCortoEn;
    }

    public String getNombreLocal() {
        return nombreLocal;
    }

    public void setNombreLocal(String nombreLocal) {
        this.nombreLocal = nombreLocal;
    }

    public String getNombreCortoLocal() {
        return nombreCortoLocal;
    }

    public void setNombreCortoLocal(String nombreCortoLocal) {
        this.nombreCortoLocal = nombreCortoLocal;
    }

    public String getNutsI() {
        return nutsI;
    }

    public void setNutsI(String nutsI) {
        this.nutsI = nutsI;
    }
}
