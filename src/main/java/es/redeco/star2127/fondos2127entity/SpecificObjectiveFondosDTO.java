package es.redeco.star2127.fondos2127entity;

public class SpecificObjectiveFondosDTO {
    public Integer id;
    public String acronimo;
    public String acronimoEn;
    public Float codigo;
    public String codigoSfc;
    public String descripcion;
    public String descripcionEn;
    public String descripcionFr;
    public String descripcionPt;
    public Boolean esAt;
    public String nombre;
    public String nombreCorto;
    public String nombreCortoEn;
    public String nombreCortoFr;
    public String nombreCortoPt;
    public String nombreEn;
    public String nombreFr;
    public String nombrePt;

    //TODO Pending adding relation to political objective


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    public String getAcronimoEn() {
        return acronimoEn;
    }

    public void setAcronimoEn(String acronimoEn) {
        this.acronimoEn = acronimoEn;
    }

    public Float getCodigo() {
        return codigo;
    }

    public void setCodigo(Float codigo) {
        this.codigo = codigo;
    }

    public String getCodigoSfc() {
        return codigoSfc;
    }

    public void setCodigoSfc(String codigoSfc) {
        this.codigoSfc = codigoSfc;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionEn() {
        return descripcionEn;
    }

    public void setDescripcionEn(String descripcionEn) {
        this.descripcionEn = descripcionEn;
    }

    public String getDescripcionFr() {
        return descripcionFr;
    }

    public void setDescripcionFr(String descripcionFr) {
        this.descripcionFr = descripcionFr;
    }

    public String getDescripcionPt() {
        return descripcionPt;
    }

    public void setDescripcionPt(String descripcionPt) {
        this.descripcionPt = descripcionPt;
    }

    public Boolean getEsAt() {
        return esAt;
    }

    public void setEsAt(Boolean esAt) {
        this.esAt = esAt;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getNombreCortoEn() {
        return nombreCortoEn;
    }

    public void setNombreCortoEn(String nombreCortoEn) {
        this.nombreCortoEn = nombreCortoEn;
    }

    public String getNombreCortoFr() {
        return nombreCortoFr;
    }

    public void setNombreCortoFr(String nombreCortoFr) {
        this.nombreCortoFr = nombreCortoFr;
    }

    public String getNombreCortoPt() {
        return nombreCortoPt;
    }

    public void setNombreCortoPt(String nombreCortoPt) {
        this.nombreCortoPt = nombreCortoPt;
    }

    public String getNombreEn() {
        return nombreEn;
    }

    public void setNombreEn(String nombreEn) {
        this.nombreEn = nombreEn;
    }

    public String getNombreFr() {
        return nombreFr;
    }

    public void setNombreFr(String nombreFr) {
        this.nombreFr = nombreFr;
    }

    public String getNombrePt() {
        return nombrePt;
    }

    public void setNombrePt(String nombrePt) {
        this.nombrePt = nombrePt;
    }
}
