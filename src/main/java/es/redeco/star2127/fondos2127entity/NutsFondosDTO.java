package es.redeco.star2127.fondos2127entity;


public class NutsFondosDTO {

    public Integer id;
    public String codigo;
    public String codigoPais;
    public String nombrePais;
    public String codigoSfc;
    public String nombre;
    public String mfpNutsII;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoPais() {
        return codigoPais;
    }

    public void setCodigoPais(String codigoPais) {
        this.codigoPais = codigoPais;
    }

    public String getNombrePais() {
        return nombrePais;
    }

    public void setNombrePais(String nombrePais) {
        this.nombrePais = nombrePais;
    }

    public String getCodigoSfc() {
        return codigoSfc;
    }

    public void setCodigoSfc(String codigoSfc) {
        this.codigoSfc = codigoSfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMfpNutsII() {
        return mfpNutsII;
    }

    public void setMfpNutsII(String mfpNutsII) {
        this.mfpNutsII = mfpNutsII;
    }
}