package es.redeco.star2127.fondos2127entity;

public class CategoryOfRegionFondosDTO {
    public Integer id;
    public String codigo;
    public String codigoEn;
    public String descripcion;
    public String descripcionEn;
    public String nombre;
    public String nombreCorto;
    public String nombreCortoEn;
    public String nombreEn;
    public Integer orden;
    public String mfpCatRegionesCcaa;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoEn() {
        return codigoEn;
    }

    public void setCodigoEn(String codigoEn) {
        this.codigoEn = codigoEn;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionEn() {
        return descripcionEn;
    }

    public void setDescripcionEn(String descripcionEn) {
        this.descripcionEn = descripcionEn;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreCorto() {
        return nombreCorto;
    }

    public void setNombreCorto(String nombreCorto) {
        this.nombreCorto = nombreCorto;
    }

    public String getNombreCortoEn() {
        return nombreCortoEn;
    }

    public void setNombreCortoEn(String nombreCortoEn) {
        this.nombreCortoEn = nombreCortoEn;
    }

    public String getNombreEn() {
        return nombreEn;
    }

    public void setNombreEn(String nombreEn) {
        this.nombreEn = nombreEn;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public String getMfpCatRegionesCcaa() {
        return mfpCatRegionesCcaa;
    }

    public void setMfpCatRegionesCcaa(String mfpCatRegionesCcaa) {
        this.mfpCatRegionesCcaa = mfpCatRegionesCcaa;
    }
}
