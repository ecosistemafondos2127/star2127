package es.redeco.star2127.fondos2127entity;

public class CNClassificationFondosDTO {
    public Integer codigo;

    public String nombre;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
