package es.redeco.star2127.fondos2127entity;

public class CategoryOfRegionByAACCFondosDTO {
    public Integer  id;
    public Integer codigoNutII;
    public String codigoNut2;
    public String codigoCatRegion;
    public String descripcion;
    public String nomCatRegion;
    public String nomNUTII;
    public Float porcentaje;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigoNutII() {
        return codigoNutII;
    }

    public void setCodigoNutII(Integer codigoNutII) {
        this.codigoNutII = codigoNutII;
    }

    public String getCodigoNut2() {
        return codigoNut2;
    }

    public void setCodigoNut2(String codigoNut2) {
        this.codigoNut2 = codigoNut2;
    }

    public String getCodigoCatRegion() {
        return codigoCatRegion;
    }

    public void setCodigoCatRegion(String codigoCatRegion) {
        this.codigoCatRegion = codigoCatRegion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNomCatRegion() {
        return nomCatRegion;
    }

    public void setNomCatRegion(String nomCatRegion) {
        this.nomCatRegion = nomCatRegion;
    }

    public String getNomNUTII() {
        return nomNUTII;
    }

    public void setNomNUTII(String nomNUTII) {
        this.nomNUTII = nomNUTII;
    }

    public Float getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Float porcentaje) {
        this.porcentaje = porcentaje;
    }
}
