package es.redeco.star2127.fondos2127entity;

public class TerritorialFocusFondosDTO {
    public Integer id;
    public String codigoEnfoque;
    public String codigoMecanismo;
    public String nombre;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigoEnfoque() {
        return codigoEnfoque;
    }

    public void setCodigoEnfoque(String codigoEnfoque) {
        this.codigoEnfoque = codigoEnfoque;
    }

    public String getCodigoMecanismo() {
        return codigoMecanismo;
    }

    public void setCodigoMecanismo(String codigoMecanismo) {
        this.codigoMecanismo = codigoMecanismo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
