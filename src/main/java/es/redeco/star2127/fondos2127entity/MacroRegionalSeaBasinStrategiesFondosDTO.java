package es.redeco.star2127.fondos2127entity;

public class MacroRegionalSeaBasinStrategiesFondosDTO {
    public Integer id;
    public String codigo;
    public String codigoSfc;
    public String nombre;
    public String nombreEn;
    public String descripcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigoSfc() {
        return codigoSfc;
    }

    public void setCodigoSfc(String codigoSfc) {
        this.codigoSfc = codigoSfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreEn() {
        return nombreEn;
    }

    public void setNombreEn(String nombreEn) {
        this.nombreEn = nombreEn;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
