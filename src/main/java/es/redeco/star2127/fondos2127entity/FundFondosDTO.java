package es.redeco.star2127.fondos2127entity;

public class FundFondosDTO {
    public Integer id;
    public String acronimo;
    public String acronimoEn;
    public String acronimoFr;
    public String acronimoPt;
    public String codigo;
    public String nombre;
    public String nombreEn;
    public String nombreFr;
    public String nombrePt;
    public String descripcion;
    public String descripcionEn;
    public String descripcionFr;
    public String descripcionPt;
    public Boolean esRdc;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    public String getAcronimoEn() {
        return acronimoEn;
    }

    public void setAcronimoEn(String acronimoEn) {
        this.acronimoEn = acronimoEn;
    }

    public String getAcronimoFr() {
        return acronimoFr;
    }

    public void setAcronimoFr(String acronimoFr) {
        this.acronimoFr = acronimoFr;
    }

    public String getAcronimoPt() {
        return acronimoPt;
    }

    public void setAcronimoPt(String acronimoPt) {
        this.acronimoPt = acronimoPt;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreEn() {
        return nombreEn;
    }

    public void setNombreEn(String nombreEn) {
        this.nombreEn = nombreEn;
    }

    public String getNombreFr() {
        return nombreFr;
    }

    public void setNombreFr(String nombreFr) {
        this.nombreFr = nombreFr;
    }

    public String getNombrePt() {
        return nombrePt;
    }

    public void setNombrePt(String nombrePt) {
        this.nombrePt = nombrePt;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionEn() {
        return descripcionEn;
    }

    public void setDescripcionEn(String descripcionEn) {
        this.descripcionEn = descripcionEn;
    }

    public String getDescripcionFr() {
        return descripcionFr;
    }

    public void setDescripcionFr(String descripcionFr) {
        this.descripcionFr = descripcionFr;
    }

    public String getDescripcionPt() {
        return descripcionPt;
    }

    public void setDescripcionPt(String descripcionPt) {
        this.descripcionPt = descripcionPt;
    }

    public Boolean getEsRdc() {
        return esRdc;
    }

    public void setEsRdc(Boolean esRdc) {
        this.esRdc = esRdc;
    }
}
