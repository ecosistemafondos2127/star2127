package es.redeco.star2127.fondos2127entity;

public class PostalCodeFondosDTO {
    public Integer id;
    public Integer codigoNUTIII;
    public String nombreNUTIII;
    public Float codigoMunicipio;
    public String nombreMunicipio;
    public Integer codigo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigoNUTIII() {
        return codigoNUTIII;
    }

    public void setCodigoNUTIII(Integer codigoNUTIII) {
        this.codigoNUTIII = codigoNUTIII;
    }

    public String getNombreNUTIII() {
        return nombreNUTIII;
    }

    public void setNombreNUTIII(String nombreNUTIII) {
        this.nombreNUTIII = nombreNUTIII;
    }

    public Float getCodigoMunicipio() {
        return codigoMunicipio;
    }

    public void setCodigoMunicipio(Float codigoMunicipio) {
        this.codigoMunicipio = codigoMunicipio;
    }

    public String getNombreMunicipio() {
        return nombreMunicipio;
    }

    public void setNombreMunicipio(String nombreMunicipio) {
        this.nombreMunicipio = nombreMunicipio;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }
}
