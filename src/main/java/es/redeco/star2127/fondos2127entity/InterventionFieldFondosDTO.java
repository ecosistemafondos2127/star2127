package es.redeco.star2127.fondos2127entity;

public class InterventionFieldFondosDTO {
    public Integer id;
    public Integer codigo;
    public String denominacion;
    public Float porcClima;
    public Float porcAmbiental;
    public String objPoliticosTodos;
    public String objPoliticosAsignados;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDenominacion() {
        return denominacion;
    }

    public void setDenominacion(String denominacion) {
        this.denominacion = denominacion;
    }

    public Float getPorcClima() {
        return porcClima;
    }

    public void setPorcClima(Float porcClima) {
        this.porcClima = porcClima;
    }

    public Float getPorcAmbiental() {
        return porcAmbiental;
    }

    public void setPorcAmbiental(Float porcAmbiental) {
        this.porcAmbiental = porcAmbiental;
    }

    public String getObjPoliticosTodos() {
        return objPoliticosTodos;
    }

    public void setObjPoliticosTodos(String objPoliticosTodos) {
        this.objPoliticosTodos = objPoliticosTodos;
    }

    public String getObjPoliticosAsignados() {
        return objPoliticosAsignados;
    }

    public void setObjPoliticosAsignados(String objPoliticosAsignados) {
        this.objPoliticosAsignados = objPoliticosAsignados;
    }
}
