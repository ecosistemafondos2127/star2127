package es.redeco.star2127.fondos2127entity;

public class EconomicActivityFondosDTO {
    public Integer id;
    public Integer codigo;
    public Integer codigoSfc;
    public String nombre;
    public String nombreEn;
    public String descripcion;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer getCodigoSfc() {
        return codigoSfc;
    }

    public void setCodigoSfc(Integer codigoSfc) {
        this.codigoSfc = codigoSfc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getNombreEn() {
        return nombreEn;
    }

    public void setNombreEn(String nombreEn) {
        this.nombreEn = nombreEn;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
}
