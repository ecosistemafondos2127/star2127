package es.redeco.star2127.services;

import es.redeco.star2127.conversion.AdministrativeLevelConversion;
import es.redeco.star2127.entity.AdministrativeLevel;
import es.redeco.star2127.entity.DataConnectionConfigurations;
import es.redeco.star2127.fondos2127entity.NutsFondosDTO;
import es.redeco.star2127.listeners.LoadDataEventListener;
import io.jmix.core.DataManager;
import io.jmix.core.querycondition.PropertyCondition;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Code based on JMIX example from
// https://github.com/jmix-framework/jmix-samples-2/blob/main/external-data-sample/src/main/java/com/company/externaldata/app/TaskService.java
// Conversion from curl to java from: https://www.scrapingbee.com/curl-converter/java/

@Component
public class FondosNutsService {
    private static final Logger log = org.slf4j.LoggerFactory.getLogger(FondosNutsService.class);

    @Autowired
    private DataManager dataManager;
    public static final String EXAMPLE_BASE_URL = "https://webpub2.igae.hacienda.gob.es/coffee-iec/countries/filter?skip=1&size=15";

    public String loadAndSaveFondoss2127URL(String authorization, String cookie) {
        try {
            DataConnectionConfigurations config = loadConfiguration("DEMO", "Country", null);

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.set(HttpHeaders.ACCEPT, "application/json, text/plain, */*");
            headers.set(HttpHeaders.ACCEPT_LANGUAGE, "es-ES,es;q=0.9");
            headers.set(HttpHeaders.AUTHORIZATION, authorization);
            headers.set(HttpHeaders.COOKIE, cookie);
            headers.set(HttpHeaders.CACHE_CONTROL, "no-cache");
            headers.set(HttpHeaders.CONNECTION, "keep-alive");
            headers.set(HttpHeaders.PRAGMA, "no-cache");
            headers.set(HttpHeaders.REFERER, "https://webpub2.igae.hacienda.gob.es/coffee-iec-front/admin/localizaciones/paises");
            //headers.set("Sec-Fetch-Dest", "empty");
            //headers.set("Sec-Fetch-Mode", "cors");
            //headers.set("Sec-Fetch-Site", "same-origin");
            //headers.set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36");

            //headers.set("sec-ch-ua", "\"Not_A Brand\";v=\"8\", \"Chromium\";v=\"120\", \"Google Chrome\";v=\"120\"");
            //headers.set("sec-ch-ua-mobile", "?0");
            //headers.set("sec-ch-ua-platform", "\"Windows\"");

            Map<String, String> params = new HashMap<String, String>();
            //params.put("applicationName", applicationName);

            HttpEntity<Void> requestEntity = new HttpEntity<>(headers);
            ResponseEntity<String> responseEntity = restTemplate.exchange(EXAMPLE_BASE_URL, HttpMethod.GET, requestEntity, String.class, params);
            String response  = responseEntity.getBody();

            log.info("Response at exchange:" + response);
            String filename = "./resultFondos2127.json";
            try (PrintWriter out = new PrintWriter(filename)) {
                out.println(response);
            } catch (FileNotFoundException e) {
                log.error("FileNotFoundException at exchange:", e);
                return "Error: " + e;
            }
            return response;
        } catch (RestClientException e) {
            log.error("RestClientException at exchange:", e);
            return "Error: " + e;
        }
    }

    public DataConnectionConfigurations loadConfiguration(String environment, String primaryConcept, String secondaryConcept) {
        try {
            return dataManager.load(DataConnectionConfigurations.class)
                    .query("select c from DataConnectionConfigurations c where c.environment = :environment and c.primaryConcept = :primary and c.secondaryConcept = :secondary")
                    .parameter("environment", environment)
                    .parameter("primary", primaryConcept)
                    .parameter("secondary", secondaryConcept)
                    .one();
        } catch (Exception e)
        {
            log.error("Configuration not found for entity:" + primaryConcept);
            //throw  e;
            return null;
        }
    }
    public List<NutsFondosDTO> loadTasks() {
        RestTemplate restTemplate = new RestTemplate();

        NutsFondosDTO[] records = restTemplate.getForObject(EXAMPLE_BASE_URL, NutsFondosDTO[].class);
        return Arrays.asList(records);
    }

    public NutsFondosDTO saveTask(NutsFondosDTO record) {
        RestTemplate restTemplate = new RestTemplate();

        String url = record.getId() != null ? EXAMPLE_BASE_URL + "/"  + record.getId() : EXAMPLE_BASE_URL;
        ResponseEntity<NutsFondosDTO> response = restTemplate.postForEntity(url, record, NutsFondosDTO.class);
        return response.getBody();
    }
}
