package es.redeco.star2127.entity;

import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

@JmixEntity
@Table(name = "ORGANISM", indexes = {
        @Index(name = "IDX_ORGANISM_LEGAL_ENTITY", columnList = "LEGAL_ENTITY_ID"),
        @Index(name = "IDX_ORGANISM_FUND", columnList = "FUND_ID"),
        @Index(name = "IDX_ORGANISM_CONTACT", columnList = "CONTACT_ID")
})
@Entity
public class Organism extends StandardBitemporalEntity {
    @InstanceName
    @Column(name = "CODE", nullable = false)
    @NotNull
    private String code;

    @Column(name = "NAME_ES", nullable = false)
    @NotNull
    private String nameES;

    @Column(name = "ORIGIN", nullable = false)
    @NotNull
    private String origin;

    @Column(name = "OBSERVATIONS", nullable = false)
    @NotNull
    private String observations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CONTACT_ID")
    private Contact contact;

    @JoinColumn(name = "FUND_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Fund fund;

    @JoinColumn(name = "LEGAL_ENTITY_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private LegalEntity legalEntity;

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public Contact getContact() {
        return contact;
    }

    public Fund getFund() {
        return fund;
    }

    public void setFund(Fund fund) {
        this.fund = fund;
    }

    public String getObservations() {
        return observations;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getNameES() {
        return nameES;
    }

    public void setNameES(String nameES) {
        this.nameES = nameES;
    }

    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}