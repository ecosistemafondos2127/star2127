package es.redeco.star2127.entity;

import io.jmix.core.metamodel.datatype.EnumClass;

import org.springframework.lang.Nullable;


public enum NutsEnum implements EnumClass<String> {

    NUTS0("0"),
    NUTS1("1"),
    NUTS2("2"),
    NUTS3("3");


    private final String id;

    NutsEnum(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static NutsEnum fromId(String id) {
        for (NutsEnum at : NutsEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}