package es.redeco.star2127.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.time.LocalDate;

@JmixEntity
@Table(name = "LEGAL_ENTITY", indexes = {
        @Index(name = "IDX_LEGAL_ENTITY_LAW_TYPE", columnList = "LAW_TYPE_ID")
})
@Entity
public class LegalEntity extends StandardBitemporalEntity {
    @NotNull
    @Column(name = "CODE", nullable = false)
    private String code;

    @Column(name = "CODE_INVENTE", nullable = false)
    @NotNull
    private String codeInvente;

    @Column(name = "CODE_DIR3", nullable = false)
    @NotNull
    private String codeDIR3;

    @Column(name = "VERSION_DIR3", nullable = false)
    @NotNull
    private String versionDIR3;

    @Column(name = "DENOMINATION", nullable = false)
    @NotNull
    private String denomination;

    @Column(name = "CODE_DIR3_FATHER", nullable = false)
    @NotNull
    private String codeDIR3Father;

    @Column(name = "VERSION_DIR3_PADRE", nullable = false)
    @NotNull
    private String versionDIR3Father;

    @Column(name = "DENOMINATION_FATHER", nullable = false)
    @NotNull
    private String denominationFather;

    @Column(name = "CODE_DIR3_ROOT", nullable = false)
    @NotNull
    private String codeDIR3Root;

    @Column(name = "VERSION_DIR3_ROOT", nullable = false)
    @NotNull
    private String versionDIR3Root;

    @Column(name = "DENOMINACION_ROOT", nullable = false)
    @NotNull
    private String denominationRoot;

    @Column(name = "NIF", nullable = false)
    @NotNull
    private String nif;

    @NotNull
    @JoinColumn(name = "COUNTRY_ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Country country;

    @Column(name = "ORIGIN", nullable = false)
    @NotNull
    private String origin;

    @Column(name = "VERSION_ORIGIN", nullable = false)
    @NotNull
    private String versionOrigin;

    @JoinColumn(name = "STATE_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private StateDIR3 state;

    @NotNull
    @JoinColumn(name = "NIF_TYPE_ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private NIFType nifType;

    @Column(name = "LEGAL_REPRESENTATIVE", nullable = false)
    @NotNull
    private String legalRepresentative;

    @Column(name = "ADMIN_LEVEL", nullable = false)
    @NotNull
    private String adminLevel;

    @NotNull
    @Column(name = "BIRTHDAY", nullable = false)
    private LocalDate birthday;

    @JoinColumn(name = "CLASSIFICATION_CN_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private CNClassification classificationCn;

    @Column(name = "HIERARCHICAL_LEVEL", nullable = false)
    @NotNull
    private String hierarchicalLevel;

    @Column(name = "LEGAL_FORM", nullable = false)
    @NotNull
    private String legalForm;

    @JoinColumn(name = "LAW_TYPE_ID", nullable = false)
    @ManyToOne(optional = false)
    @NotNull
    private LawType lawType;

    @NotNull
    @Column(name = "APPLY_LAW", nullable = false)
    private Boolean applyLaw = false;

    @NotNull
    @JoinColumn(name = "LEGAL_TYPE_ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private LegalType legalType;

    public String getLegalForm() {
        return legalForm;
    }

    public void setLegalForm(String legalForm) {
        this.legalForm = legalForm;
    }

    public LegalType getLegalType() {
        return legalType;
    }

    public void setLegalType(LegalType legalType) {
        this.legalType = legalType;
    }


    public void setLawType(LawType lawType) {
        this.lawType = lawType;
    }

    public LawType getLawType() {
        return lawType;
    }

    public void setClassificationCn(CNClassification classificationCn) {
        this.classificationCn = classificationCn;
    }

    public CNClassification getClassificationCn() {
        return classificationCn;
    }

    public void setNifType(NIFType nifType) {
        this.nifType = nifType;
    }

    public NIFType getNifType() {
        return nifType;
    }

    public void setState(StateDIR3 state) {
        this.state = state;
    }

    public StateDIR3 getState() {
        return state;
    }

    public void setApplyLaw(Boolean applyLaw) {
        this.applyLaw = applyLaw;
    }

    public Boolean getApplyLaw() {
        return applyLaw;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Country getCountry() {
        return country;
    }

    public String getHierarchicalLevel() {
        return hierarchicalLevel;
    }

    public void setHierarchicalLevel(String hierarchicalLevel) {
        this.hierarchicalLevel = hierarchicalLevel;
    }

    public String getAdminLevel() {
        return adminLevel;
    }

    public void setAdminLevel(String adminLevel) {
        this.adminLevel = adminLevel;
    }

    public String getLegalRepresentative() {
        return legalRepresentative;
    }

    public void setLegalRepresentative(String legalRepresentative) {
        this.legalRepresentative = legalRepresentative;
    }

    public String getVersionOrigin() {
        return versionOrigin;
    }

    public void setVersionOrigin(String versionOrigin) {
        this.versionOrigin = versionOrigin;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getNif() {
        return nif;
    }

    public void setNif(String nif) {
        this.nif = nif;
    }

    public String getDenominationRoot() {
        return denominationRoot;
    }

    public void setDenominationRoot(String denominationRoot) {
        this.denominationRoot = denominationRoot;
    }

    public String getVersionDIR3Root() {
        return versionDIR3Root;
    }

    public void setVersionDIR3Root(String versionDIR3Root) {
        this.versionDIR3Root = versionDIR3Root;
    }

    public String getCodeDIR3Root() {
        return codeDIR3Root;
    }

    public void setCodeDIR3Root(String codeDIR3Root) {
        this.codeDIR3Root = codeDIR3Root;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public String getDenominationFather() {
        return denominationFather;
    }

    public void setDenominationFather(String denominationFather) {
        this.denominationFather = denominationFather;
    }

    public String getVersionDIR3Father() {
        return versionDIR3Father;
    }

    public void setVersionDIR3Father(String versionDIR3Father) {
        this.versionDIR3Father = versionDIR3Father;
    }

    public String getVersionDIR3() {
        return versionDIR3;
    }

    public void setVersionDIR3(String versionDIR3) {
        this.versionDIR3 = versionDIR3;
    }

    public String getCodeDIR3Father() {
        return codeDIR3Father;
    }

    public void setCodeDIR3Father(String codeDIR3Father) {
        this.codeDIR3Father = codeDIR3Father;
    }

    public String getCodeDIR3() {
        return codeDIR3;
    }

    public void setCodeDIR3(String codeDIR3) {
        this.codeDIR3 = codeDIR3;
    }

    public String getCodeInvente() {
        return codeInvente;
    }

    public void setCodeInvente(String codeInvente) {
        this.codeInvente = codeInvente;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}