package es.redeco.star2127.entity;

import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

import java.util.List;

@JmixEntity
@Table(name = "INDICATOR_", indexes = {
        @Index(name = "IDX_INDICATOR__ASSIGNATED_FUND", columnList = "ASSIGNATED_FUND_ID"),
        @Index(name = "IDX_INDICATOR__INDICATOR_UNIT_MEASUREMENT", columnList = "INDICATOR_UNIT_MEASUREMENT_ID")
})
@Entity(name = "Indicator_")
public class Indicator extends StandardBitemporalEntity {
    @Column(name = "CODE", nullable = false)
    @NotNull
    private String code;

    @Column(name = "CODE_SFC")
    private String codeSfc;

    @Column(name = "CODE_EM")
    private String codeEm;

    @Column(name = "DENOMINATION_ES", nullable = false)
    @NotNull
    private String denominationES;

    @Column(name = "DENOMINATION_EN")
    private String denominationEN;

    @Column(name = "DEFINITION_ES")
    private String definitionES;

    @Column(name = "DEFINITION_EN")
    private String definitionEN;

    @Column(name = "CLASS_INDICATOR", nullable = false)
    @NotNull
    private String classIndicator;

    @NotNull
    @Column(name = "AMBIT", nullable = false)
    private String ambit;

    @Column(name = "TYPE_", nullable = false)
    @NotNull
    private String type;

    @Column(name = "DEFAULT_VALUE")
    private String defaultValue;

    @Column(name = "MILESTONE2024")
    private String milestone2024;

    @Column(name = "GOAL2029")
    private String goal2029;

    @Column(name = "DATA_COLLECTION")
    private String dataCollection;

    @Column(name = "RULES_AGGREGATION")
    private String rulesAggregation;

    @Column(name = "RULES_REPORTING")
    private String rulesReporting;

    @Column(name = "REFERENCIES")
    private String referencies;

    @Column(name = "IND_CORPORATE")
    private String indCorporate;

    @Column(name = "NOTES")
    private String notes;

    @Column(name = "INI_YEAR")
    private String iniYear;

    @Column(name = "END_YEAR")
    private String endYear;

    @JoinColumn(name = "INDICATOR_UNIT_MEASUREMENT_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Unit_measurement indicatorUnitMeasurement;

    @JoinColumn(name = "ASSIGNATED_FUND_ID")
    @ManyToOne(fetch = FetchType.LAZY)
    private Fund assignatedFund;

    @JoinTable(name = "INDICATOR_SPECIFIC_OBJECTIVE_LINK",
            joinColumns = @JoinColumn(name = "INDICATOR_ID"),
            inverseJoinColumns = @JoinColumn(name = "SPECIFIC_OBJECTIVE_ID"))
    @ManyToMany
    private List<SpecificObjective> speObjAssignated;

    public String getCodeEm() {
        return codeEm;
    }

    public void setCodeEm(String codeEm) {
        this.codeEm = codeEm;
    }

    public Unit_measurement getIndicatorUnitMeasurement() {
        return indicatorUnitMeasurement;
    }

    public void setIndicatorUnitMeasurement(Unit_measurement indicatorUnitMeasurement) {
        this.indicatorUnitMeasurement = indicatorUnitMeasurement;
    }

    public List<SpecificObjective> getSpeObjAssignated() {
        return speObjAssignated;
    }

    public void setSpeObjAssignated(List<SpecificObjective> speObjAssignated) {
        this.speObjAssignated = speObjAssignated;
    }

    public Fund getAssignatedFund() {
        return assignatedFund;
    }

    public void setAssignatedFund(Fund assignatedFund) {
        this.assignatedFund = assignatedFund;
    }

    public String getEndYear() {
        return endYear;
    }

    public void setEndYear(String endYear) {
        this.endYear = endYear;
    }

    public String getIniYear() {
        return iniYear;
    }

    public void setIniYear(String iniYear) {
        this.iniYear = iniYear;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getIndCorporate() {
        return indCorporate;
    }

    public void setIndCorporate(String indCorporate) {
        this.indCorporate = indCorporate;
    }

    public String getReferencies() {
        return referencies;
    }

    public void setReferencies(String referencies) {
        this.referencies = referencies;
    }

    public String getRulesReporting() {
        return rulesReporting;
    }

    public void setRulesReporting(String rulesReporting) {
        this.rulesReporting = rulesReporting;
    }

    public String getRulesAggregation() {
        return rulesAggregation;
    }

    public void setRulesAggregation(String rulesAggregation) {
        this.rulesAggregation = rulesAggregation;
    }

    public String getDataCollection() {
        return dataCollection;
    }

    public void setDataCollection(String dataCollection) {
        this.dataCollection = dataCollection;
    }

    public String getGoal2029() {
        return goal2029;
    }

    public void setGoal2029(String goal2029) {
        this.goal2029 = goal2029;
    }

    public String getMilestone2024() {
        return milestone2024;
    }

    public void setMilestone2024(String milestone2024) {
        this.milestone2024 = milestone2024;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAmbit() {
        return ambit;
    }

    public void setAmbit(String ambit) {
        this.ambit = ambit;
    }

    public String getClassIndicator() {
        return classIndicator;
    }

    public void setClassIndicator(String classIndicator) {
        this.classIndicator = classIndicator;
    }

    public String getDefinitionEN() {
        return definitionEN;
    }

    public void setDefinitionEN(String definitionEN) {
        this.definitionEN = definitionEN;
    }

    public String getDefinitionES() {
        return definitionES;
    }

    public void setDefinitionES(String definitionES) {
        this.definitionES = definitionES;
    }

    public String getDenominationEN() {
        return denominationEN;
    }

    public void setDenominationEN(String denominationEN) {
        this.denominationEN = denominationEN;
    }

    public String getCodeSfc() {
        return codeSfc;
    }

    public void setCodeSfc(String codeSfc) {
        this.codeSfc = codeSfc;
    }

    public String getDenominationES() {
        return denominationES;
    }

    public void setDenominationES(String denominationES) {
        this.denominationES = denominationES;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}