package es.redeco.star2127.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@JmixEntity
@Table(name = "SPECIFIC_OBJECTIVE", indexes = {
        @Index(name = "IDX_SPECIFIC_OBJECTIVE_POLICY_OBJECTIVE", columnList = "POLICY_OBJECTIVE_ID")
})
@Entity
public class SpecificObjective {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @JoinColumn(name = "POLICY_OBJECTIVE_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private PoliticalObjective policyObjective;

    @Column(name = "ACRONYM_ES", nullable = false, length = 5)
    @NotNull
    private String acronymES;

    @Column(name = "ACRONYM_EN", length = 5)
    private String acronymEN;

    @Column(name = "CODE", nullable = false)
    @NotNull
    private Integer code;

    @NotNull
    @Column(name = "CODE_SFC", nullable = false)
    private Integer codeSfc;

    @Length(min = 0, max = 255)
    @Column(name = "DESCRIPTION_ES")
    private String descriptionES;

    @Length(min = 0, max = 255)
    @Column(name = "DESCRIPTION_EN")
    private String descriptionEN;

    @Length(min = 0, max = 255)
    @Column(name = "DESCRIPTION_FR")
    private String descriptionFR;

    @Length(min = 0, max = 255)
    @Column(name = "DESCRIPTION_PT")
    private String descriptionPT;

    @Column(name = "SHORT_NAME_ES", nullable = false, length = 10)
    @NotNull
    private String shortNameES;

    @Column(name = "SHORT_NAME_EN", length = 10)
    private String shortNameEN;

    @Column(name = "SHORT_NAME_FR", length = 10)
    private String shortNameFR;

    @Column(name = "SHORT_NAME_PT", length = 10)
    private String shortNamePT;

    @InstanceName
    @Column(name = "NAME_ES", nullable = false, length = 50)
    @NotNull
    private String nameES;

    @Column(name = "NAME_EN", length = 50)
    private String nameEN;

    @Column(name = "NAME_PT", length = 50)
    private String namePT;

    @Column(name = "NAME_FR", length = 50)
    private String nameFR;

    @Column(name = "IS_TA", nullable = false)
    @NotNull
    private Boolean isTa = false;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;
    @JoinTable(name = "INDICATOR_SPECIFIC_OBJECTIVE_LINK",
            joinColumns = @JoinColumn(name = "SPECIFIC_OBJECTIVE_ID"),
            inverseJoinColumns = @JoinColumn(name = "INDICATOR_ID"))
    @ManyToMany
    private List<Indicator> indicators;

    public List<Indicator> getIndicators() {
        return indicators;
    }

    public void setIndicators(List<Indicator> indicators) {
        this.indicators = indicators;
    }

    public void setCodeSfc(Integer codeSfc) {
        this.codeSfc = codeSfc;
    }

    public Integer getCodeSfc() {
        return codeSfc;
    }

    public String getNameFR() {
        return nameFR;
    }

    public void setNameFR(String nameFR) {
        this.nameFR = nameFR;
    }

    public String getNamePT() {
        return namePT;
    }

    public void setNamePT(String namePT) {
        this.namePT = namePT;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getShortNamePT() {
        return shortNamePT;
    }

    public void setShortNamePT(String shortNamePT) {
        this.shortNamePT = shortNamePT;
    }

    public String getShortNameFR() {
        return shortNameFR;
    }

    public void setShortNameFR(String shortNameFR) {
        this.shortNameFR = shortNameFR;
    }

    public String getShortNameEN() {
        return shortNameEN;
    }

    public void setShortNameEN(String shortNameEN) {
        this.shortNameEN = shortNameEN;
    }

    public String getShortNameES() {
        return shortNameES;
    }

    public void setShortNameES(String shortNameES) {
        this.shortNameES = shortNameES;
    }

    public String getNameES() {
        return nameES;
    }

    public void setNameES(String nameES) {
        this.nameES = nameES;
    }

    public Boolean getIsTa() {
        return isTa;
    }

    public void setIsTa(Boolean isTa) {
        this.isTa = isTa;
    }

    public String getDescriptionPT() {
        return descriptionPT;
    }

    public void setDescriptionPT(String descriptionPT) {
        this.descriptionPT = descriptionPT;
    }

    public String getDescriptionFR() {
        return descriptionFR;
    }

    public void setDescriptionFR(String descriptionFR) {
        this.descriptionFR = descriptionFR;
    }

    public String getDescriptionEN() {
        return descriptionEN;
    }

    public void setDescriptionEN(String descriptionEN) {
        this.descriptionEN = descriptionEN;
    }

    public String getDescriptionES() {
        return descriptionES;
    }

    public void setDescriptionES(String descriptionES) {
        this.descriptionES = descriptionES;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getAcronymEN() {
        return acronymEN;
    }

    public void setAcronymEN(String acronymEN) {
        this.acronymEN = acronymEN;
    }

    public String getAcronymES() {
        return acronymES;
    }

    public void setAcronymES(String acronymES) {
        this.acronymES = acronymES;
    }

    public PoliticalObjective getPolicyObjective() {
        return policyObjective;
    }

    public void setPolicyObjective(PoliticalObjective policyObjective) {
        this.policyObjective = policyObjective;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}