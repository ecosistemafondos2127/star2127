package es.redeco.star2127.entity;

import io.jmix.core.metamodel.datatype.EnumClass;

import org.springframework.lang.Nullable;


public enum EditionStatus implements EnumClass<String> {

    OPEN_MAJOR("Major edition"),
    OPEN_MINOR("Minor edition"),
    VALID("Valid"),
    NOT_VALID("Not valid");

    private final String id;

    EditionStatus(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static EditionStatus fromId(String id) {
        for (EditionStatus at : EditionStatus.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}