package es.redeco.star2127.entity;

import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;

@JmixEntity
@Table(name = "VALIDATION")
@Entity
public class Validation extends StandardBitemporalEntity {

    @NotNull
    @Column(name = "FUNDS2127_ID", nullable = false)
    private Integer funds2127Id;

    @NotNull
    @Column(name = "CODE", nullable = false, length = 10)
    private String code;

    @JoinColumn(name = "LEVEL_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Constant level;

    @InstanceName
    @Column(name = "DESCRIPTION", nullable = false)
    @NotNull
    private String description;

    @Column(name = "MESSAGE", nullable = false)
    @NotNull
    private String message;

    @Column(name = "ORDER_")
    private String order;

    @JoinColumn(name = "ACTIVE_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Constant active;

    @JoinColumn(name = "AUTOMATIC_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Constant automatic;

    @Column(name = "LOGIC_LANGUAJE")
    private String logicLanguaje;

    @Column(name = "LOGIC_PROCESSABLE")
    private String logicProcessable;

    @JoinColumn(name = "PROCESS_CONTINOUS_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Constant processContinous;

    @Column(name = "LANGUAGE_LOGIC_BACK")
    private String languageLogicBack;

    @Column(name = "LOGIC_PROCESSABLE_BACK")
    private String logicProcessableBack;

    @Column(name = "LANGUAGE_LOGIC_FRONT")
    private String languageLogicFront;

    @Column(name = "LOGIC_PROCESSABLE_FRONT")
    private String logicProcessableFront;

    @Column(name = "ID_FUND_GROUP", nullable = false)
    @NotNull
    private String idFundGroup;

    @Column(name = "FUND_CODE", nullable = false)
    @NotNull
    private String fundCode;

    @JoinColumn(name = "DESCR_FUND_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Constant descrFund;

    @Column(name = "CODE_GROUP", nullable = false)
    @NotNull
    private String codeGroup;

    @Column(name = "DESCR_GROUP", nullable = false)
    @NotNull
    private String descrGroup;

    @Column(name = "ID_ENTITY_TYPE", nullable = false)
    @NotNull
    private String idEntityType;

    @JoinColumn(name = "CODE_ENTITY_TYPE_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private EntityType codeEntityType;

    @Column(name = "STATE", nullable = false)
    @NotNull
    private String state;

    @Column(name = "ORIGIN_VERSION")
    private String originVersion;

    @NotNull
    @Column(name = "CAN_EDIT", nullable = false)
    private Boolean canEdit = false;

    public void setCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setProcessContinous(Constant processContinous) {
        this.processContinous = processContinous;
    }

    public Constant getProcessContinous() {
        return processContinous;
    }

    public void setAutomatic(Constant automatic) {
        this.automatic = automatic;
    }

    public Constant getAutomatic() {
        return automatic;
    }

    public void setActive(Constant active) {
        this.active = active;
    }

    public Constant getActive() {
        return active;
    }

    public void setCodeEntityType(EntityType codeEntityType) {
        this.codeEntityType = codeEntityType;
    }

    public EntityType getCodeEntityType() {
        return codeEntityType;
    }

    public void setDescrFund(Constant descrFund) {
        this.descrFund = descrFund;
    }

    public Constant getDescrFund() {
        return descrFund;
    }

    public void setLevel(Constant level) {
        this.level = level;
    }

    public Constant getLevel() {
        return level;
    }

    public void setCanEdit(Boolean canEdit) {
        this.canEdit = canEdit;
    }

    public Boolean getCanEdit() {
        return canEdit;
    }

    public void setFunds2127Id(Integer funds2127Id) {
        this.funds2127Id = funds2127Id;
    }

    public Integer getFunds2127Id() {
        return funds2127Id;
    }

    public String getOriginVersion() {
        return originVersion;
    }

    public void setOriginVersion(String originVersion) {
        this.originVersion = originVersion;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getIdEntityType() {
        return idEntityType;
    }

    public void setIdEntityType(String idEntityType) {
        this.idEntityType = idEntityType;
    }

    public String getDescrGroup() {
        return descrGroup;
    }

    public void setDescrGroup(String descrGroup) {
        this.descrGroup = descrGroup;
    }

    public String getCodeGroup() {
        return codeGroup;
    }

    public void setCodeGroup(String codeGroup) {
        this.codeGroup = codeGroup;
    }

    public String getFundCode() {
        return fundCode;
    }

    public void setFundCode(String fundCode) {
        this.fundCode = fundCode;
    }

    public String getIdFundGroup() {
        return idFundGroup;
    }

    public void setIdFundGroup(String idFundGroup) {
        this.idFundGroup = idFundGroup;
    }

    public String getLogicProcessableFront() {
        return logicProcessableFront;
    }

    public void setLogicProcessableFront(String logicProcessableFront) {
        this.logicProcessableFront = logicProcessableFront;
    }

    public String getLanguageLogicFront() {
        return languageLogicFront;
    }

    public void setLanguageLogicFront(String languageLogicFront) {
        this.languageLogicFront = languageLogicFront;
    }

    public String getLogicProcessableBack() {
        return logicProcessableBack;
    }

    public void setLogicProcessableBack(String logicProcessableBack) {
        this.logicProcessableBack = logicProcessableBack;
    }

    public String getLanguageLogicBack() {
        return languageLogicBack;
    }

    public void setLanguageLogicBack(String languageLogicBack) {
        this.languageLogicBack = languageLogicBack;
    }

    public String getLogicProcessable() {
        return logicProcessable;
    }

    public void setLogicProcessable(String logicProcessable) {
        this.logicProcessable = logicProcessable;
    }

    public String getLogicLanguaje() {
        return logicLanguaje;
    }

    public void setLogicLanguaje(String logicLanguaje) {
        this.logicLanguaje = logicLanguaje;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}