package es.redeco.star2127.entity;

import io.jmix.core.metamodel.datatype.EnumClass;

import org.springframework.lang.Nullable;


public enum Environment implements EnumClass<String> {

    DEMO("M"),
    DES("DE"),
    PRE("PR"),
    PRO("P");

    private final String id;

    Environment(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static Environment fromId(String id) {
        for (Environment at : Environment.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}