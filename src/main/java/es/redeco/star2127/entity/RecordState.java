package es.redeco.star2127.entity;

import io.jmix.core.metamodel.datatype.EnumClass;

import org.springframework.lang.Nullable;


public enum RecordState implements EnumClass<String> {

    UNKNOWN("State of the record is unknown "),
    FONDOS2127_CURRENT("Fondos2127 has current value"),
    STAR_CURRENT("Star has current value"),
    BOTH_CURRENT("Both have current value"),
    FONDOS2127_EDITED("Record has been edited in Fondos2127"),
    STAR_EDITED("Record has been edited in STAR"),
    BOTH_EDITED("Record has been edited inconsistently in both systems"),
    DELETED_FONDOS2127("Record has been deleted in Fondos2127"),
    DELETED_STAR("Record has been deleted in STAR"),
    DELETED_BOTH("Record has been deleted in both systems"),
    EXISTS_ONLY_IN_FONDOS2127("Record exists only in Fondos2127"),
    EXISTS_ONLY_IN_STAR("Record exists only in STAR");

    private final String id;

    RecordState(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static RecordState fromId(String id) {
        for (RecordState at : RecordState.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}