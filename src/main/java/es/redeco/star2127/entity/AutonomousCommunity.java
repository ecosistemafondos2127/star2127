package es.redeco.star2127.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity
@Table(name = "AUTONOMOUS_COMMUNITIES", indexes = {
        @Index(name = "IDX_AUTONOMOUS_COMMUNITY_COUNTRIES", columnList = "COUNTRY_ID"),
        @Index(name = "IDX_AUTONOMOUS_COMMUNITY_NUTS", columnList = "CODE_NUTS2_ID")
})
@Entity
public class AutonomousCommunity {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @NotNull
    @Column(name = "FUNDS2127ID", nullable = false)
    private Integer funds2127id;

    @NotNull
    @Column(name = "ISO_CODE31662", nullable = false)
    private String isoCode31662;

    @NotNull
    @Column(name = "ACRONYM_ES", nullable = false, length = 5)
    private String acronymES;

    @NotNull
    @Column(name = "NAME_ES", nullable = false, length = 50)
    private String nameES;

    @NotNull
    @Column(name = "NAME_EN", nullable = false, length = 50)
    private String nameEN;

    @JoinColumn(name = "COUNTRY_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Country country;

    @JoinColumn(name = "CODE_NUTS2_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Nuts codeNuts2;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public void setCodeNuts2(Nuts codeNuts2) {
        this.codeNuts2 = codeNuts2;
    }

    public Nuts getCodeNuts2() {
        return codeNuts2;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameES(String nameES) {
        this.nameES = nameES;
    }

    public String getNameES() {
        return nameES;
    }

    public void setAcronymES(String acronymES) {
        this.acronymES = acronymES;
    }

    public String getAcronymES() {
        return acronymES;
    }

    public void setFunds2127id(Integer funds2127id) {
        this.funds2127id = funds2127id;
    }

    public Integer getFunds2127id() {
        return funds2127id;
    }

    public void setIsoCode31662(String isoCode31662) {
        this.isoCode31662 = isoCode31662;
    }

    public String getIsoCode31662() {
        return isoCode31662;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}