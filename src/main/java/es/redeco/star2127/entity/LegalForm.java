package es.redeco.star2127.entity;

import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

import java.util.UUID;

@JmixEntity
@Table(name = "LEGAL_FORM")
@Entity
public class LegalForm {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "FUNDS2127_ID", nullable = false)
    @NotNull
    private String funds2127Id;

    @Column(name = "CODE", nullable = false)
    @NotNull
    private String code;

    @Column(name = "NAME_ES", nullable = false)
    @NotNull
    private String nameES;

    public String getNameES() {
        return nameES;
    }

    public void setNameES(String nameES) {
        this.nameES = nameES;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getFunds2127Id() {
        return funds2127Id;
    }

    public void setFunds2127Id(String funds2127Id) {
        this.funds2127Id = funds2127Id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}