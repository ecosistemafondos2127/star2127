package es.redeco.star2127.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity
@Table(name = "CATEGORY_OF_REGION_BY_AACC")
@Entity
public class CategoryOfRegionByAACC {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "FUNDS2127ID", nullable = false)
    @NotNull
    private Integer funds2127id;

    @Column(name = "CODE_NUT_II", nullable = false)
    @NotNull
    private Integer codeNutII;

    @Column(name = "CODE_NUT2", nullable = false)
    @NotNull
    private String codeNut2;

    @Column(name = "CODE_REGION_CAT", nullable = false)
    @NotNull
    private String codeRegionCat;

    @Column(name = "DESCRIPTION_ES")
    private String descriptionES;

    @Column(name = "NAME_REGION_CAT", nullable = false)
    @NotNull
    private String nameRegionCat;

    @Column(name = "NAME_NUT_II", nullable = false)
    @NotNull
    private String nameNutII;

    @NotNull
    @Column(name = "PERCENTAGE", nullable = false)
    private Float percentage;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;

    public Integer getFunds2127id() {
        return funds2127id;
    }

    public void setFunds2127id(Integer funds2127id) {
        this.funds2127id = funds2127id;
    }

    public Float getPercentage() {
        return percentage;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }

    public String getNameNutII() {
        return nameNutII;
    }

    public void setNameNutII(String nameNutII) {
        this.nameNutII = nameNutII;
    }

    public String getNameRegionCat() {
        return nameRegionCat;
    }

    public void setNameRegionCat(String nameRegionCat) {
        this.nameRegionCat = nameRegionCat;
    }

    public String getDescriptionES() {
        return descriptionES;
    }

    public void setDescriptionES(String descriptionES) {
        this.descriptionES = descriptionES;
    }

    public String getCodeRegionCat() {
        return codeRegionCat;
    }

    public void setCodeRegionCat(String codeRegionCat) {
        this.codeRegionCat = codeRegionCat;
    }

    public String getCodeNut2() {
        return codeNut2;
    }

    public void setCodeNut2(String codeNut2) {
        this.codeNut2 = codeNut2;
    }

    public Integer getCodeNutII() {
        return codeNutII;
    }

    public void setCodeNutII(Integer codeNutII) {
        this.codeNutII = codeNutII;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}