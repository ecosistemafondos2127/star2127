package es.redeco.star2127.entity;

import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;

@JmixEntity
@Table(name = "BITEMPORAL_EXAMPLE")
@Entity
public class BitemporalExample extends StandardBitemporalEntity{

    public static void copyRegs(BitemporalExample from, BitemporalExample to) {
        StandardBitemporalEntity.copyRegs(from, to);
        to.setSomeValue(from.getSomeValue());
        to.setDescription(from.getDescription());
    }

    @NotNull
    @Column(name = "SOME_VALUE_")
    private Double someValue;

    @InstanceName
    @Column(name = "DESCRIPTION")
    private String description;

    public Double getSomeValue() {
        return someValue;
    }

    public void setSomeValue(Double someValue) {
        this.someValue = someValue;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}