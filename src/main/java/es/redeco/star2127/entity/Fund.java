package es.redeco.star2127.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity
@Table(name = "FUND")
@Entity
public class Fund {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "ACRONYM_ES", nullable = false, length = 5)
    @NotNull
    private String acronymES;

    @Column(name = "ACRONYM_EN", length = 5)
    private String acronymEN;

    @Column(name = "ACRONYM_FR", length = 5)
    private String acronymFR;

    @Column(name = "ACRONYM_PT", length = 5)
    private String acronymPT;

    @NotNull
    @Column(name = "CODE", nullable = false)
    private Integer code;

    @InstanceName
    @Column(name = "NAME_ES", nullable = false, length = 50)
    @NotNull
    private String nameES;

    @Column(name = "NAME_EN", length = 50)
    private String nameEN;

    @Column(name = "NAME_FR", length = 50)
    private String nameFR;

    @Column(name = "NAME_PT", length = 50)
    private String namePT;

    @Column(name = "DESCRIPTION_ES", nullable = false)
    @NotNull
    private String descriptionES;

    @Column(name = "DESCRIPTION_EN")
    private String descriptionEN;

    @Column(name = "DESCRIPTION_FR")
    private String descriptionFR;

    @Column(name = "DESCRIPTION_PT")
    private String descriptionPT;

    @Column(name = "IS_CDR", nullable = false)
    @NotNull
    private Boolean isCDR = false;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public Boolean getIsCDR() {
        return isCDR;
    }

    public void setIsCDR(Boolean isCDR) {
        this.isCDR = isCDR;
    }

    public String getDescriptionPT() {
        return descriptionPT;
    }

    public void setDescriptionPT(String descriptionPT) {
        this.descriptionPT = descriptionPT;
    }

    public String getDescriptionFR() {
        return descriptionFR;
    }

    public void setDescriptionFR(String descriptionFR) {
        this.descriptionFR = descriptionFR;
    }

    public String getDescriptionEN() {
        return descriptionEN;
    }

    public void setDescriptionEN(String descriptionEN) {
        this.descriptionEN = descriptionEN;
    }

    public String getDescriptionES() {
        return descriptionES;
    }

    public void setDescriptionES(String descriptionES) {
        this.descriptionES = descriptionES;
    }

    public String getNamePT() {
        return namePT;
    }

    public void setNamePT(String namePT) {
        this.namePT = namePT;
    }

    public String getNameFR() {
        return nameFR;
    }

    public void setNameFR(String nameFR) {
        this.nameFR = nameFR;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameES() {
        return nameES;
    }

    public void setNameES(String nameES) {
        this.nameES = nameES;
    }

    public String getAcronymPT() {
        return acronymPT;
    }

    public void setAcronymPT(String acronymPT) {
        this.acronymPT = acronymPT;
    }

    public String getAcronymFR() {
        return acronymFR;
    }

    public void setAcronymFR(String acronymFR) {
        this.acronymFR = acronymFR;
    }

    public String getAcronymEN() {
        return acronymEN;
    }

    public void setAcronymEN(String acronymEN) {
        this.acronymEN = acronymEN;
    }

    public String getAcronymES() {
        return acronymES;
    }

    public void setAcronymES(String acronymES) {
        this.acronymES = acronymES;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}