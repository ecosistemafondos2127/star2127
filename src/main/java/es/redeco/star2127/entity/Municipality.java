package es.redeco.star2127.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity
@Table(name = "MUNICIPALITIES", indexes = {
        @Index(name = "IDX_MUNICIPALITY_PROVINCIES", columnList = "PROCINCY_ID"),
        @Index(name = "IDX_MUNICIPALITY_NUTS", columnList = "CODE_NUTS3_ID")
})
@Entity
public class Municipality {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @NotNull
    @Column(name = "FUNDS2127ID", nullable = false)
    private Integer funds2127id;

    @NotNull
    @Column(name = "EXTERNAL_CODE", nullable = false, length = 5)
    private String externalCode;

    @InstanceName
    @NotNull
    @Column(name = "NAME_ES", nullable = false, length = 50)
    private String nameES;

    @NotNull
    @Column(name = "NAME_EN", nullable = false, length = 50)
    private String nameEN;

    @NotNull
    @Column(name = "CODE_INE", nullable = false, length = 5)
    private String codeINE;

    @JoinColumn(name = "CODE_NUTS3_ID", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Nuts codeNuts3;

    @NotNull
    @JoinColumn(name = "PROCINCY_ID", nullable = false)
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Province province;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;

    public void setProvince(Province autoCom) {
        this.province = autoCom;
    }

    public Province getProvince() {
        return province;
    }

    public void setCodeNuts3(Nuts codeNuts3) {
        this.codeNuts3 = codeNuts3;
    }

    public Nuts getCodeNuts3() {
        return codeNuts3;
    }

    public void setCodeINE(String codeINE) {
        this.codeINE = codeINE;
    }

    public String getCodeINE() {
        return codeINE;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameES(String nameES) {
        this.nameES = nameES;
    }

    public String getNameES() {
        return nameES;
    }

    public void setExternalCode(String externalCode) {
        this.externalCode = externalCode;
    }

    public String getExternalCode() {
        return externalCode;
    }

    public void setFunds2127id(Integer funds2127id) {
        this.funds2127id = funds2127id;
    }

    public Integer getFunds2127id() {
        return funds2127id;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}