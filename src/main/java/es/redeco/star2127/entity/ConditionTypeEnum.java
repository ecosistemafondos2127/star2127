package es.redeco.star2127.entity;

import io.jmix.core.metamodel.datatype.EnumClass;

import org.springframework.lang.Nullable;


public enum ConditionTypeEnum implements EnumClass<String> {

    HORIZONTAL("1"),
    THEMATIC("2");

    private final String id;

    ConditionTypeEnum(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Nullable
    public static ConditionTypeEnum fromId(String id) {
        for (ConditionTypeEnum at : ConditionTypeEnum.values()) {
            if (at.getId().equals(id)) {
                return at;
            }
        }
        return null;
    }
}