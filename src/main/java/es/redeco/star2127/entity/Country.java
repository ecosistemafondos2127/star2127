package es.redeco.star2127.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.InstanceName;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity
@Table(name = "COUNTRIES", indexes = {
        @Index(name = "IDX_COUNTRY_COUNTRY_NUTS", columnList = "CODE_NUTS1")
})
@Entity
public class Country {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @NotNull
    @Column(name = "FUNDS2127", nullable = false)
    private Integer funds2127id;

    @InstanceName
    @NotNull
    @Column(name = "NAME_ES", nullable = false, length = 50)
    private String nameES;

    @Column(name = "SHORT_NAME_ES", length = 10)
    private String shortNameES;

    @Column(name = "NAME_EN", length = 50)
    private String nameEN;

    @Column(name = "SHORT_NAME_EN", length = 10)
    private String shortNameEN;

    @JoinColumn(name = "CODE_NUTS1", nullable = false)
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Nuts codeNUTS1;

    @Length(message = "{msg://es.redeco.star2127.entity/Country.isoCodeChar2.validation.Length}", min = 2, max = 2)
    @NotNull
    @Column(name = "ISO_CODE_ALPHA2", nullable = false, length = 2)
    private String isoCodeAlpha2;

    @NotNull
    @Column(name = "ISO_CODE_ALPHA3", nullable = false, length = 3)
    private String isoCodeAlpha3;

    @Column(name = "IS_EU", nullable = false)
    @NotNull
    private Boolean isEU = false;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    public void setCodeNUTS1(Nuts codeNUTS1) {
        this.codeNUTS1 = codeNUTS1;
    }

    public Nuts getCodeNUTS1() {
        return codeNUTS1;
    }

    public void setIsoCodeAlpha3(String isoCodeAlpha3) {
        this.isoCodeAlpha3 = isoCodeAlpha3;
    }

    public String getIsoCodeAlpha3() {
        return isoCodeAlpha3;
    }

    public void setIsoCodeAlpha2(String isoCodeAlpha2) {
        this.isoCodeAlpha2 = isoCodeAlpha2;
    }

    public String getIsoCodeAlpha2() {
        return isoCodeAlpha2;
    }

    public void setFunds2127id(Integer funds2127id) {
        this.funds2127id = funds2127id;
    }

    public Integer getFunds2127id() {
        return funds2127id;
    }

    public void setNameES(String nameES) {
        this.nameES = nameES;
    }

    public String getNameES() {
        return nameES;
    }

    public Boolean getIsEU() {
        return isEU;
    }

    public void setIsEU(Boolean isEU) {
        this.isEU = isEU;
    }

    public String getShortNameEN() {
        return shortNameEN;
    }

    public void setShortNameEN(String shortNameEN) {
        this.shortNameEN = shortNameEN;
    }

    public String getNameEN() {
        return nameEN;
    }

    public void setNameEN(String nameEN) {
        this.nameEN = nameEN;
    }

    public String getShortNameES() {
        return shortNameES;
    }

    public void setShortNameES(String shortNameES) {
        this.shortNameES = shortNameES;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}