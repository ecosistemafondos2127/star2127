package es.redeco.star2127.entity;

import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity
@Table(name = "INTERVENTION_FIELD")
@Entity
public class InterventionField {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Column(name = "CODE", nullable = false)
    @NotNull
    private Integer code;

    @Column(name = "DENOMINATION", nullable = false)
    @NotNull
    private String denomination;

    @Column(name = "PERCENTAGE_CLIMATE", nullable = false)
    @NotNull
    private Float percentageClimate;

    @Column(name = "PERCENTAGE_ENVIRONMENT", nullable = false)
    @NotNull
    private Float percentageEnvironment;

    @Column(name = "ALL_POLITICAL_OBJECTIVES")
    private String allPoliticalObjectives;

    @Column(name = "ASIGNED_POLITICAL_OBJECTIVE")
    private String asignedPoliticalObjective;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;

    public String getAsignedPoliticalObjective() {
        return asignedPoliticalObjective;
    }

    public void setAsignedPoliticalObjective(String asignedPoliticalObjective) {
        this.asignedPoliticalObjective = asignedPoliticalObjective;
    }

    public String getAllPoliticalObjectives() {
        return allPoliticalObjectives;
    }

    public void setAllPoliticalObjectives(String allPoliticalObjectives) {
        this.allPoliticalObjectives = allPoliticalObjectives;
    }

    public Float getPercentageEnvironment() {
        return percentageEnvironment;
    }

    public void setPercentageEnvironment(Float percentageEnvironment) {
        this.percentageEnvironment = percentageEnvironment;
    }

    public Float getPercentageClimate() {
        return percentageClimate;
    }

    public void setPercentageClimate(Float percentageClimate) {
        this.percentageClimate = percentageClimate;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}