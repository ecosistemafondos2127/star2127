package es.redeco.star2127.entity;

import es.redeco.star2127.temporalversioning.BitemporalEntity;
import io.jmix.core.annotation.DeletedBy;
import io.jmix.core.annotation.DeletedDate;
import io.jmix.core.entity.annotation.JmixGeneratedValue;
import io.jmix.core.metamodel.annotation.JmixEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Version;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.UUID;

@JmixEntity //TODO: review
@MappedSuperclass
public class StandardBitemporalEntity implements BitemporalEntity {
    @JmixGeneratedValue
    @Column(name = "ID", nullable = false)
    @Id
    private UUID id;

    @Pattern(regexp = "^[0-9]*(\\.[0-9]*)*$")
    @Column(name = "VERSION_NUMBER", nullable = false)
    @NotNull
    private String versionNumber;

    public static void copyRegs(StandardBitemporalEntity from, StandardBitemporalEntity to) {
        to.setEditVersion(from.getEditVersion());
        to.setRecordStatus(from.getRecordStatus());
        to.setVersionNumber(from.getVersionNumber());
        to.setTt_Start(from.getTt_Start());
        to.setTt_End(from.getTt_End());
        to.setVt_Start(from.getVt_Start());
        to.setVt_End(from.getVt_End());
    }

    @NotNull
    @Column(name = "RECORD_STATUS", nullable = false)
    private String recordStatus;

    @NotNull
    @Column(name = "EDIT_VERSION", nullable = false)
    private String editVersion;

    @NotNull
    @Column(name = "VT_START", nullable = false)
    private LocalDateTime vt_Start;

    @Column(name = "VT_END", nullable = false)
    private LocalDateTime vt_End;

    @NotNull
    @Column(name = "TT_START", nullable = false)
    private LocalDateTime tt_Start;

    @Column(name = "TT_END", nullable = false)
    private LocalDateTime tt_End;

    @Column(name = "VERSION", nullable = false)
    @Version
    private Integer version;

    @CreatedBy
    @Column(name = "CREATED_BY")
    private String createdBy;

    @CreatedDate
    @Column(name = "CREATED_DATE")
    private OffsetDateTime createdDate;

    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    private OffsetDateTime lastModifiedDate;

    @DeletedBy
    @Column(name = "DELETED_BY")
    private String deletedBy;

    @DeletedDate
    @Column(name = "DELETED_DATE")
    private OffsetDateTime deletedDate;


    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public void setEditVersion(EditionStatus editVersion) {
        this.editVersion = editVersion == null ? null : editVersion.getId();
    }

    public EditionStatus getEditVersion() {
        return editVersion == null ? null : EditionStatus.fromId(editVersion);
    }

    public void setRecordStatus(RecordState recordStatus) {
        this.recordStatus = recordStatus == null ? null : recordStatus.getId();
    }

    public RecordState getRecordStatus() {
        return recordStatus == null ? null : RecordState.fromId(recordStatus);
    }

    public void setTt_End(LocalDateTime tt_End) {
        this.tt_End = tt_End;
    }

    public void setTt_Start(LocalDateTime tt_Start) {
        this.tt_Start = tt_Start;
    }

    public void setVt_Start(LocalDateTime vt_Start) {
        this.vt_Start = vt_Start;
    }

    public void setVt_End(LocalDateTime vt_End) {
        this.vt_End = vt_End;
    }

    public LocalDateTime getVt_Start() {
        return vt_Start;
    }

    public LocalDateTime getVt_End() {
        return vt_End;
    }

    public LocalDateTime getTt_Start() {
        return tt_Start;
    }

    public LocalDateTime getTt_End() {
        return tt_End;
    }

    public OffsetDateTime getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(OffsetDateTime deletedDate) {
        this.deletedDate = deletedDate;
    }

    public String getDeletedBy() {
        return deletedBy;
    }

    public void setDeletedBy(String deletedBy) {
        this.deletedBy = deletedBy;
    }

    public OffsetDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    public void setLastModifiedDate(OffsetDateTime lastModifiedDate) {
        this.lastModifiedDate = lastModifiedDate;
    }

    public String getLastModifiedBy() {
        return lastModifiedBy;
    }

    public void setLastModifiedBy(String lastModifiedBy) {
        this.lastModifiedBy = lastModifiedBy;
    }

    public OffsetDateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(OffsetDateTime createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setEffectivePeriod(LocalDateTime from, LocalDateTime to) {
        setVt_Start(from);
        setVt_End(to);
    }

    public void setRecordPeriod(LocalDateTime from, LocalDateTime to) {
        setTt_Start(from);
        setTt_End(to);
    }
}